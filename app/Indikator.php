<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indikator extends Model
{
    protected $table = 'indikator';
    protected $fillable = [
        'kinerja', 'bobot', 'target', 'detailTarget', 'status', 'golonganId'
    ];
    public $timestamps = false;

    // fk
    public function golongan(){
        return $this->belongsTo('App\Golongan', 'golonganId');
    }

    public function detailPenilaian(){
        return $this->hasMany('App\DetailPenilaian', 'Id');
    }
}
