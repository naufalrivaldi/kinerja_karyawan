<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Penilai extends Authenticatable
{
    use Notifiable;
    protected $table = 'penilai';
    protected $fillable = [
        'nama', 'nik', 'password', 'remember_token', 'levelId', 'divisiId'
    ];
    protected $hidden = [
        'password', 'remember_token'
    ];

    public $timestamps = false;

    // fk
    public function level(){
        return $this->belongsTo('App\Level', 'levelId');
    }

    public function divisi(){
        return $this->belongsTo('App\Divisi', 'divisiId');
    }

    public function penilaian(){
        return $this->hasMany('App\Penilaian', 'penilaiId');
    }

    public function penilaiApprove(){
        return $this->hasOne('App\Penilaian', 'penilaiApprove');
    }
}
