<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Golongan;
use App\Periode;
use App\Divisi;
use App\Penilaian;

use Auth;
use PDF;

class LaporanPenilaianController extends Controller
{
    public function index(){
        $karyawanId = [];
        $level = $this->setLevel(Auth()->user()->levelId);
        $data['no'] = 1;
        $data['periode'] = Periode::orderBy('id', 'desc')->get();
        $data['divisi'] = Divisi::orderBy('namaDivisi', 'asc')->get();
        $data['golongan'] = Golongan::orderBy('nama', 'asc')->get();

        // logikanya ribet bangettt hmmmm
        if($_GET){
            $periode = Periode::where('id', 'like', '%'.$_GET['periodeId'].'%')->first();
            $penilaian = Penilaian::whereHas('karyawan', function($query) use ($level){
                $query->whereIn('levelId', $level);
            })->where('periodeId', $periode->id)->where('status', '>', '2')->get();

            foreach($penilaian as $penilaian){
                if(Auth::guard('penilai')->user()->levelId < 3){
                    if($penilaian->karyawan->golongan->divisiId == Auth::guard('penilai')->user()->divisiId){
                        array_push($karyawanId, $penilaian->karyawanId);    
                    }
                }else{
                    array_push($karyawanId, $penilaian->karyawanId);
                }
            }

            $data['setPeriode'] = $periode;

            if($_GET['golonganId'] == '0'){
                $data['setGolongan'] = Golongan::whereHas('karyawan', function($query) use ($karyawanId){
                    $query->whereIn('id', $karyawanId);
                })->whereHas('divisi', function($query){
                    $query->where('id', $_GET['divisiId']);
                })->orderBy('nama', 'asc')->get();
            }else{
                $data['setGolongan'] = Golongan::whereHas('karyawan', function($query) use ($karyawanId){
                    $query->whereIn('id', $karyawanId);
                })->where('id', 'like', '%'.$_GET['golonganId'].'%')->orderBy('nama', 'asc')->get();
            }

            $data['penilaian'] = Penilaian::where('periodeId', $periode->id)->get();
            $data['golonganEdit'] = Golongan::where('divisiId', $_GET['divisiId'])->get();
        }else{
            $periode = Periode::orderBy('id', 'desc')->first();
            // jika periode tidak ada
            if(!empty($periode)){
                $penilaian = Penilaian::whereHas('karyawan', function($query) use ($level){
                    $query->whereIn('levelId', $level);
                })->where('periodeId', $periode->id)->where('status', '>', '2')->get();
    
                foreach($penilaian as $penilaian){
                    if(Auth::guard('penilai')->user()->levelId < 3){
                        if($penilaian->karyawan->golongan->divisiId == Auth::guard('penilai')->user()->divisiId){
                            array_push($karyawanId, $penilaian->karyawanId);    
                        }
                    }else{
                        array_push($karyawanId, $penilaian->karyawanId);
                    }
                }
    
                $data['setPeriode'] = $periode;
                $data['setGolongan'] = Golongan::whereHas('karyawan', function($query) use ($karyawanId){
                    $query->whereIn('id', $karyawanId);
                })->orderBy('nama', 'asc')->get();
                $data['penilaian'] = Penilaian::where('periodeId', $periode->id)->get();
            }else{
                $data['setPeriode'] = null;
            }
        }

        return view('laporan.penilaian.index', $data);
    }

    public function print(){
        $karyawanId = [];
        $level = $this->setLevel(Auth()->user()->levelId);
        $data['no'] = 1;
        $data['periode'] = Periode::orderBy('id', 'desc')->get();
        $data['divisi'] = Divisi::orderBy('namaDivisi', 'asc')->get();
        $data['golongan'] = Golongan::orderBy('nama', 'asc')->get();

        // logikanya ribet bangettt hmmmm
        if($_GET){
            $periode = Periode::where('id', 'like', '%'.$_GET['periodeId'].'%')->first();
            $penilaian = Penilaian::whereHas('karyawan', function($query) use ($level){
                $query->whereIn('levelId', $level);
            })->where('periodeId', $periode->id)->get();

            foreach($penilaian as $penilaian){
                array_push($karyawanId, $penilaian->karyawanId);
            }

            $data['setPeriode'] = $periode;

            $data['setGolongan'] = Golongan::whereHas('karyawan', function($query) use ($karyawanId){
                $query->whereIn('id', $karyawanId);
            })->where('id', 'like', '%'.$_GET['golonganId2'].'%')->orderBy('nama', 'asc')->get();

            $data['penilaian'] = Penilaian::where('periodeId', $periode->id)->get();
            $data['golonganEdit'] = Golongan::where('divisiId', $_GET['divisiId'])->get();
        }else{
            $periode = Periode::orderBy('id', 'desc')->first();

            $penilaian = Penilaian::whereHas('karyawan', function($query) use ($level){
                $query->whereIn('levelId', $level);
            })->where('periodeId', $periode->id)->get();

            foreach($penilaian as $penilaian){
                array_push($karyawanId, $penilaian->karyawanId);
            }

            $data['setPeriode'] = $periode;
            $data['setGolongan'] = Golongan::whereHas('karyawan', function($query) use ($karyawanId){
                $query->whereIn('id', $karyawanId);
            })->orderBy('nama', 'asc')->get();
            $data['penilaian'] = Penilaian::where('periodeId', $periode->id)->get();
        }
        
        $pdf = PDF::loadview('laporan.penilaian.print', $data)->setPaper('a4', 'portrait');
        return $pdf->download('laporan-penilaian.pdf');

        // return view('laporan.penilaian.print', $data);
    }

    public function golongan(){
        $text = '<option value="">Pilih</option><option value="0">Semua Golongan</option>';
        $divisiId = $_GET['id'];
        $golongan = Golongan::where('divisiId', $divisiId)->get();
        
        foreach($golongan as $golongan){
            $text .= "<option value='".$golongan->id."'>".$golongan->nama."</option>";
        }

        return $text;
    }

    private function setLevel($levelId){
        $level = '';
        switch ($levelId) {
            case 2:
                $level = array('1');
                break;

            case 3:
                $level = array('1', '2');
                break;

            case 4:
                $level = array('1', '2', '3');
                break;

            case 5:
                $level = array('1', '2', '3', '4', '5');
                break;
            
            default:
                # code...
                break;
        }

        return $level;
    }
}
