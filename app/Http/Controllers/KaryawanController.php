<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KaryawanRequest;

use App\Karyawan;
use App\Divisi;
use App\Level;
use App\Golongan;
use App\Penilaian;

use PDF;

// use App\Helpers\MyHelper;

class KaryawanController extends Controller
{
    public function index(){
        $divisiId = '';
        $status = '';
        $data['no'] = 1;
        
        if($_GET){
            $divisiId = $_GET['divisiId'];
            $status = $_GET['status'];
        }

        $data['karyawan'] = Karyawan::whereHas('golongan', function($query) use ($divisiId){
            $query->where('divisiId', $divisiId);
        })->where('status', 'like', '%'.$status.'%')->orderBy('golonganId', 'asc')->get();

        if($divisiId == ''){
            $data['karyawan'] = Karyawan::where('status', 'like', '%'.$status.'%')->orderBy('golonganId', 'asc')->get();
        }elseif($status == ''){
            $data['karyawan'] = Karyawan::whereHas('golongan', function($query) use ($divisiId){
                $query->where('divisiId', $divisiId);
            })->orderBy('golonganId', 'asc')->get();
        }

        $data['divisi'] = Divisi::all();
        return view('karyawan.index', $data);
    }

    public function form(){
        $data['divisi'] = Divisi::orderBy('inisial', 'asc')->get();
        $data['level'] = Level::all();
        $data['golongan'] = Golongan::all();
        return view('karyawan.form', $data);
    }

    public function edit($id){
        $data['id'] = $id;
        $data['karyawan'] = Karyawan::find($id);
        $data['divisi'] = Divisi::orderBy('inisial', 'asc')->get();
        $data['level'] = Level::all();
        $data['golongan'] = Golongan::all();
        return view('karyawan.form', $data);
    }

    public function detail($id){
        $arrayNilai = array();
        $arrayPeriode = array();
        $data['id'] = $id;
        $data['no'] = 1;
        $data['karyawan'] = Karyawan::find($id);
        $data['penilaian'] = Penilaian::where('karyawanId', $id)->where('status', 3)->orderBy('periodeId', 'desc')->get();
        $penilaian = Penilaian::where('karyawanId', $id)->where('status', 3)->orderBy('periodeId', 'asc')->get();

        if(!empty($penilaian)){
            foreach($penilaian as $row){
                $sum = sum($row->detailPenilaian);
                array_push($arrayNilai, $sum);
                array_push($arrayPeriode, $row->periode->nama);
            }
        }

        $data['arrayNilai'] = $arrayNilai;
        $data['arrayPeriode'] = $arrayPeriode;

        return view('karyawan.detail', $data);
    }

    public function print($id){
        $data['no'] = 1;
        $data['penilaian'] = Penilaian::find($id);

        $pdf = PDF::loadview('karyawan.print', $data)->setPaper('a4', 'portrait');
        return $pdf->download('laporan-penilaian-perkaryawan.pdf');
        // return view('karyawan.print', $data);
    }

    public function view(){
        $penilaianId = $_GET['penilaianId'];

        $data['no'] = 1;
        $data['penilaian'] = Penilaian::find($penilaianId);

        return view('karyawan.view', $data);
    }

    public function golongan(){
        $text = '<option value="">Pilih</option>';
        $divisiId = $_GET['id'];
        $golongan = Golongan::where('divisiId', $divisiId)->get();
        
        foreach($golongan as $golongan){
            $text .= "<option value='".$golongan->id."'>".$golongan->nama."</option>";
        }

        return $text;
    }

    public function golonganEdit(){
        $text = '<option value="">Pilih</option>';
        $attr = '';
        $divisiId = $_GET['divisiId'];
        $golonganId = $_GET['golonganId'];
        $golongan = Golongan::where('divisiId', $divisiId)->get();
        
        foreach($golongan as $golongan){
            if($golongan->id == $golonganId){
                $attr = 'selected';
            }
            $text .= "<option value='".$golongan->id."' ".$attr.">".$golongan->nama."</option>";

            $attr = '';
        }

        return $text;
    }

    public function store(KaryawanRequest $req){
        Karyawan::create([
            'nik' => $req->nik,
            'nama' => $req->nama,
            'jk' => $req->jk,
            'status' => '1',
            'golonganId' => $req->golonganId,
            'levelId' => $req->levelId
        ]);

        return redirect()->route('karyawan')->with('success', 'Data berhasil di tambahkan.');
    }

    public function update(KaryawanRequest $req){
        $data = Karyawan::find($req->id);
        $data->nik = $req->nik;
        $data->nama = $req->nama;
        $data->jk = $req->jk;
        $data->golonganId = $req->golonganId;
        $data->levelId = $req->levelId;
        $data->save();

        return redirect()->route('karyawan')->with('success', 'Data berhasil update.');
    }

    public function destroy(Request $req){
        $data = Karyawan::find($req->id);
        $data->delete();
        
        return redirect()->route('karyawan')->with('success', 'Data berhasil dihapus.');
    }

    public function aktif($id){
        $data = Karyawan::find($id);
        $data->status = '1';
        $data->save();

        return redirect()->route('karyawan')->with('success', 'Ubah status menjadi Aktif.');
    }

    public function nonaktif($id){
        $data = Karyawan::find($id);
        $data->status = '2';
        $data->save();

        return redirect()->route('karyawan')->with('success', 'Ubah status menjadi Nonaktif.');
    }
}
