<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PenilaianRequest;

use App\Periode;
use App\Penilaian;
use App\Golongan;
use App\Karyawan;
use App\Divisi;
use App\DetailPenilaian;

use Auth;

class PenilaianController extends Controller
{
    public function index(){
        $now = date('Y-m-d');
        $periode = Periode::where('status', '1')->first();
        $level = Auth::guard('penilai')->user()->levelId;
        
        // Level SPV 
        if($level < 3){
            $karyawan = Karyawan::whereHas('Golongan', function($query){
                $query->where('divisiId', Auth::guard('penilai')->user()->divisiId);
            })->where('levelId', setLevel(Auth::guard('penilai')->user()->levelId))->where('status', 1)->get();
    
            $golongan = Karyawan::select('golonganId')->whereHas('Golongan', function($query){
                $query->where('divisiId', Auth::guard('penilai')->user()->divisiId);
            })->where('levelId', setLevel(Auth::guard('penilai')->user()->levelId))->groupBy('golonganId')->get();
        }else{
            if($level == 5){
                $karyawan = Karyawan::where('status', 1)->get();
    
                $golongan = Karyawan::select('golonganId')->groupBy('golonganId')->get();
            }else{
                $karyawan = Karyawan::where('levelId', setLevel(Auth::guard('penilai')->user()->levelId))->where('status', 1)->get();
    
                $golongan = Karyawan::select('golonganId')->where('levelId', setLevel(Auth::guard('penilai')->user()->levelId))->groupBy('golonganId')->get();
            }
        }
        
        $data['no'] = 1;
        $data['periode'] = $periode;
        $data['dateNow'] = $now;
        $data['karyawan'] = $karyawan;
        $data['golongan'] = $golongan;
        
        // jika periode tidak ada
        if(!empty($periode)){
            $data['penilaian'] = Penilaian::where('periodeId', $periode->id)->where('penilaiId', Auth::guard('penilai')->user()->id)->get();
        }else{
            $data['penilaian'] = null;
        }

        return view('penilaian.index', $data);
    }

    public function form($karyawanId){
        $data['karyawan'] = Karyawan::find($karyawanId);
        
        return view('penilaian.form', $data);
    }

    public function edit($karyawanId){
        $karyawan = Karyawan::find($karyawanId);
        $periode = Periode::where('status', '1')->first();
        $data['karyawan'] = $karyawan;
        
        $penilaian = Penilaian::where('periodeId', $periode->id)->where('karyawanId', $karyawanId)->first();
        if(!empty($penilaian)){
            $data['id'] = $penilaian->id;
            $data['penilaian'] = $penilaian;
        }
        
        return view('penilaian.form', $data);
    }

    public function store(PenilaianRequest $req){
        $dateNow = date('Y-m-d');
        $periode = Periode::where('status', '1')->first();

        Penilaian::create([
            'tglPenilaian' => $dateNow,
            'periodeId' => $periode->id,
            'penilaiId' => Auth::guard('penilai')->user()->id,
            'karyawanId' => $req->karyawanId,
            'status' => 1
        ]);
        
        $penilaian = Penilaian::orderBy('id', 'desc')->first();
        for($i=0; $i<count($req->indikatorId); $i++){
            DetailPenilaian::create([
                'penilaianId' => $penilaian->id,
                'indikatorId' => $req->indikatorId[$i],
                'realisasi' => $req->realisasi[$i],
                'keterangan' => $req->keterangan[$i]
            ]);
        }

        return redirect()->route('penilaian')->with('success', 'Data berhasil di simpan.');
    }

    public function kirim($periodeId, $golonganId){
        $data = Penilaian::where('periodeId', $periodeId)->where('penilaiId', Auth::guard('penilai')->user()->id)->whereHas('karyawan', function($query) use ($golonganId){
            $query->where('golonganId', $golonganId);
        })->get();
        
        foreach($data as $data){
            $data->status = 2;
            $data->save();
        }

        return redirect()->route('penilaian')->with('success', 'Kirim data berhasil.');
    }

    public function update(PenilaianRequest $req){
        $dateNow = date('Y-m-d');
        $penilaian = Penilaian::find($req->id);
        $penilaian->tglPenilaian = $dateNow;
        $penilaian->save();
        
        for($i=0; $i<count($req->indikatorId); $i++){
            $detailPenilaian = DetailPenilaian::where('penilaianId', $req->penilaianId)->where('indikatorId', $req->indikatorId[$i])->update([
                'realisasi' => $req->realisasi[$i], 'keterangan' => $req->keterangan[$i]
            ]);
        }

        return redirect()->route('penilaian')->with('success', 'Data berhasil di update.');
    }

    public function view(){
        $karyawanId = $_GET['karyawanId'];
        $periodeId = $_GET['periodeId'];

        $data['no'] = 1;
        $data['penilaian'] = Penilaian::where('karyawanId', $karyawanId)->where('periodeId', $periodeId)->first();

        return view('penilaian.view', $data);
    }
}
