<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\IndikatorRequest;

use App\Divisi;
use App\Golongan;
use App\Indikator;

class IndikatorController extends Controller
{
    public function index(){
        $data['no'] = 1;
        $data['divisi'] = Divisi::orderBy('namaDivisi', 'asc')->get();

        return view('indikator.index', $data);
    }

    public function view(){
        $id = $_GET['id'];
        $data['no'] = 1;
        $data['golongan'] = Golongan::where('divisiId', $id)->get();

        return view('indikator.view', $data);
    }

    public function form($id){
        $data['divisi'] = Divisi::orderBy('inisial', 'asc')->get();
        $data['golongan'] = Golongan::find($id);
        return view('indikator.form', $data);
    }

    public function edit($id){
        $data['id'] = $id;
        $data['divisi'] = Divisi::orderBy('inisial', 'asc')->get();
        $data['golongan'] = Golongan::find($id);

        return view('indikator.form', $data);
    }

    public function golongan(){
        $text = '<option value="">Pilih</option>';
        $divisiId = $_GET['id'];
        $golongan = Golongan::where('divisiId', $divisiId)->get();
        
        foreach($golongan as $golongan){
            if(empty($golongan->indikator)){
                $text .= "<option value='".$golongan->id."'>".$golongan->nama."</option>";
            }
        }

        return $text;
    }

    public function store(IndikatorRequest $req){
        if(isset($req->kinerja)){
            for($i = 0; $i < count($req->kinerja); $i++){
                Indikator::create([
                    'kinerja' => $req->kinerja[$i],
                    'bobot' => $req->bobot[$i],
                    'target' => $req->target[$i],
                    'detailTarget' => $req->detailTarget[$i],
                    'status' => 1,
                    'golonganId' => $req->golonganId
                ]);
            }

            return redirect()->route('indikator')->with('success', 'Penambahan indikator berhasil.');
        }

        return redirect()->route('indikator')->with('error', 'Penambahan indikator gagal!');
    }

    public function update(IndikatorRequest $req){
        if(isset($req->kinerja)){
            $data = Indikator::where('golonganId', $req->golonganId)->where('status', 1)->get();
            foreach($data as $data){
                $data->status = 2;
                $data->save();
            }

            // logika update data indikator
            for($i = 0; $i < count($req->kinerja); $i++){
                // jika kosong input indiaktor lagi
                if(empty($req->indikatorId[$i])){
                    Indikator::create([
                        'kinerja' => $req->kinerja[$i],
                        'bobot' => $req->bobot[$i],
                        'target' => $req->target[$i],
                        'detailTarget' => $req->detailTarget[$i],
                        'status' => 1,
                        'golonganId' => $req->golonganId
                    ]);
                }else{
                    // kalau tidak kosong edit recored sebelumnya
                    $data = Indikator::find($req->indikatorId[$i]);
                    $data->kinerja = $req->kinerja[$i];
                    $data->bobot = $req->bobot[$i];
                    $data->target = $req->target[$i];
                    $data->detailTarget = $req->detailTarget[$i];
                    $data->status = 1;
                    $data->save();
                }
            }

            return redirect()->route('indikator')->with('success', 'Penambahan indikator berhasil.');
        }

        return redirect()->route('indikator')->with('error', 'Penambahan indikator gagal!');
    }

    public function nonaktif(){
        $id = $_GET['id'];
        $data = Indikator::where('golonganId', $id)->get();
        foreach($data as $data){
            $data->status = 2;
            $data->save();
        }
    }
}
