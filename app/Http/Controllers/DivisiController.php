<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DivisiRequest;

use App\Divisi;

class DivisiController extends Controller
{
    public function index(){
        $data['no'] = 1;
        $data['divisi'] = Divisi::orderBy('inisial', 'asc')->get();
        return view('divisi.index', $data);
    }

    public function form(){
        $data['id'] = '';
        return view('divisi.form', $data);
    }

    public function edit($id){
        $data['id'] = $id;
        $data['divisi'] = Divisi::find($id);
        return view('divisi.form', $data);
    }

    public function store(DivisiRequest $req){
        Divisi::create($req->all());

        return redirect()->route('divisi')->with('success', 'Divisi berhasil ditambahkan.');
    }

    public function update(DivisiRequest $req){
        $data = Divisi::find($req->id);
        $data->inisial = $req->inisial;
        $data->namaDivisi = $req->namaDivisi;
        $data->save();

        return redirect()->route('divisi')->with('success', 'Divisi berhasil di update.');
    }

    public function destroy(Request $req){
        $data = Divisi::find($req->id);
        $data->delete();
    }
}
