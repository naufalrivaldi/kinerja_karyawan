<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Periode;
use App\Golongan;
use App\Karyawan;
use App\Penilaian;

use Auth;

class VerifikasiController extends Controller
{
    public function index(){
        $level = Auth()->user()->levelId;
        $data['no'] = 1;
        $data['periode'] = Periode::orderBy('id', 'desc')->get();

        switch ($level) {
            case 2:
                $data['golongan'] = Golongan::whereHas('karyawan', function($query){
                    $query->where('levelId', 1);
                })->orderBy('nama', 'asc')->get();
                break;

            case 3:
                $data['golongan'] = Golongan::whereHas('karyawan', function($query){
                    $query->whereIn('levelId', ['1', '2']);
                })->orderBy('nama', 'asc')->get();
                break;

            case 4:
                $data['golongan'] = Golongan::whereHas('karyawan', function($query){
                    $query->where('levelId', 3);
                })->orderBy('nama', 'asc')->get();
                break;

            case 5:
                $data['golongan'] = Golongan::orderBy('nama', 'asc')->get();
                break;
            
            default:
                # code...
                break;
        }

        if($_GET){
            $data['setPeriode'] = Periode::find($_GET['periodeId']);
        }else{
            $data['setPeriode'] = Periode::orderBy('id', 'desc')->first();
        }
        
        return view('verifikasi.index', $data);
    }

    public function view($golonganId, $periodeId){
        $data['no'] = 1;
        $data['setPeriode'] = Periode::find($periodeId);
        $data['golongan'] = Golongan::find($golonganId);
        // $data['karyawan'] = Karyawan::where('golonganId', $golonganId)->get();
        $data['penilaian'] = Penilaian::whereHas('karyawan', function($query) use ($golonganId){
            $query->where('golonganId', $golonganId)->where('status', 1);
        })->where('periodeId', $periodeId)->where('status', '>', 1)->get();
        
        return view('verifikasi.view', $data);
    }

    public function viewPenilaian(){
        $penilaianId = $_GET['penilaianId'];

        $data['no'] = 1;
        $data['penilaian'] = Penilaian::find($penilaianId);

        return view('verifikasi.viewPenilaian', $data);
    }

    public function acc(Request $req){
        $data = Penilaian::find($req->penilaianId);
        $data->status = 3;
        $data->save();
    }

    public function accArray(Request $req){
        foreach($req->penilaianId as $id){
            $data = Penilaian::find($id);
            $data->status = 3;
            $data->save();
        }
    }
}
