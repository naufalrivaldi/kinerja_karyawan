<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GolonganRequest;

use App\Golongan;
use App\Divisi;

class GolonganController extends Controller
{
    public function index(){
        $data['no'] = 1;
        $data['golongan'] = Golongan::groupBy('divisiId')->get();
        return view('golongan.index', $data);
    }

    public function form(){
        $data['id'] = '';
        $data['divisi'] = Divisi::orderBy('inisial', 'asc')->get();
        return view('golongan.form', $data);
    }

    public function detail($id){
        $data['no'] = 1;
        $data['divisi'] = Divisi::find($id);
        $data['golongan'] = Golongan::where('divisiId', $id)->orderBy('nama', 'asc')->get();
        return view('golongan.detail', $data);
    }

    public function edit($id){
        $data['id'] = $id;
        $data['golongan'] = Golongan::find($id);
        $data['divisi'] = Divisi::orderBy('inisial', 'asc')->get();

        return view('golongan.form', $data);
    }

    public function store(GolonganRequest $req){
        Golongan::create($req->all());

        return redirect()->route('golongan')->with('success', 'Golongan berhasil ditambahkan.');
    }

    public function update(GolonganRequest $req){
        $data = Golongan::find($req->id);
        $data->nama = $req->nama;
        $data->bonusGaji = $req->bonusGaji;
        $data->save();

        return redirect()->route('golongan')->with('success', 'Golongan berhasil diupdate.');
    }

    public function destroy(Request $req){
        $data = Golongan::find($req->id);
        $data->delete();
    }
}
