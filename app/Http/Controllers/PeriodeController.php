<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PeriodeRequest;

use App\Periode;

class PeriodeController extends Controller
{
    public function index(){
        $data['no'] = 1;
        $data['periode'] = Periode::orderBy('tglAwal', 'desc')->get();
        
        return view('periode.index', $data);
    }

    public function form(){
        return view('periode.form');
    }

    public function edit($id){
        $data['id'] = $id;
        $data['periode'] = Periode::find($id);
        return view('periode.form', $data);
    }

    public function store(PeriodeRequest $req){
        $status = '';
        if(!empty($req->status)){
            $status = '1';
            $this->changeNonaktif();
        }else{
            $status = '2';
        }
        
        Periode::create([
            'nama' => $req->nama,
            'tglAwal' => $req->tglAwal,
            'tglAkhir' => $req->tglAkhir,
            'status' => $status
        ]);

        return redirect()->route('periode')->with('Success', 'Data berhasil ditambahkan');
    }

    public function update(PeriodeRequest $req){
        $status = '';
        if(!empty($req->status)){
            $status = '1';
            $this->changeNonaktif();
        }else{
            $status = '2';
        }
        
        $data = Periode::find($req->id);
        $data->nama = $req->nama;
        $data->tglAwal = $req->tglAwal;
        $data->tglAkhir = $req->tglAkhir;
        $data->status = $status;
        $data->save();

        return redirect()->route('periode')->with('Success', 'Data berhasil diupdate.');
    }

    public function destroy(Request $req){
        $data = Periode::find($req->id);
        $data->delete();

        return redirect()->route('periode')->with('Success', 'Data berhasil didelete.');
    }

    public function aktif($id){
        $this->changeNonaktif();

        $data = Periode::find($id);
        $data->status = '1';
        $data->save();

        return redirect()->route('periode')->with('Success', 'Status berhasil di ubah.');
    }

    public function nonaktif($id){
        $data = Periode::where('status', '1')->first();
        $data->status = '2';
        $data->save();

        return redirect()->route('periode')->with('Success', 'Status berhasil di ubah.');
    }

    public function changeNonaktif(){
        $data = Periode::where('status', '1')->first();
        if(!empty($data)){
            $data->status = '2';
            $data->save();
        }
    }
}
