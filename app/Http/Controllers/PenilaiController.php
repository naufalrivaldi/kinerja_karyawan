<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PenilaiRequest;

use App\Penilai;
use App\Divisi;
use App\Level;

class PenilaiController extends Controller
{
    public function index(){
        $data['no'] = 1;
        $data['penilai'] = Penilai::orderBy('divisiId')->get();
        
        return view('penilai.index', $data);
    }

    public function form(){
        $data['id'] = '';
        $data['divisi'] = Divisi::orderBy('inisial', 'asc')->get();
        $data['level'] = Level::all();
        return view('penilai.form', $data);
    }

    public function edit($id){
        $data['id'] = $id;
        $data['penilai'] = Penilai::find($id);
        $data['divisi'] = Divisi::orderBy('inisial', 'asc')->get();
        $data['level'] = Level::all();
        return view('penilai.form', $data);
    }

    public function reset($id){
        $id = $id;
        $data = Penilai::find($id);
        $data->password = bcrypt('123456');
        $data->save();

        return redirect()->route('penilai')->with('success', 'Password berhasil di reset.');
    }

    public function store(PenilaiRequest $req){
        Penilai::create([
            'nik' => $req->nik,
            'nama' => $req->nama,
            'password' => bcrypt('123456'),
            'remember_token' => str_random(60),
            'levelId' => $req->levelId,
            'divisiId' => $req->divisiId
        ]);

        return redirect()->route('penilai')->with('success', 'Penilai berhasil ditambahkan.');
    }

    public function update(PenilaiRequest $req){
        $data = Penilai::find($req->id);
        $data->nama = $req->nama;
        $data->nik = $req->nik;
        $data->levelId = $req->levelId;
        $data->divisiId = $req->divisiId;
        $data->save();

        return redirect()->route('penilai')->with('success', 'Penilai berhasil di update.');
    }

    public function destroy(Request $req){
        $data = Penilai::find($req->id);
        $data->delete();
    }
}
