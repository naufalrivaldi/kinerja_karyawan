<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Karyawan;
use App\Periode;

class DashboardController extends Controller
{
    public function index(){
        $data['dateNow'] = date('Y-m-d');
        $data['jmlKaryawan'] = Karyawan::all()->count();
        $data['periode'] = Periode::where('status', '1')->first();
        return view('dashboard', $data);
    }
}
