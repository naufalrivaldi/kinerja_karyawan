<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use App\Penilai;

use Illuminate\Http\Request;
use App\Http\Requests\Login;
use App\Http\Requests\UbahPasswordRequest;

class AuthController extends Controller
{
    public function index(){
        return view('login');
    }

    public function signin(Login $req){
        if(Auth::guard('penilai')->attempt($req->only('nik', 'password'))){
            return redirect()->route('dashboard')->with('success', 'Selamat datang '.Auth::guard('penilai')->user()->nama.', selamat bekerja.');
        }

        return redirect()->route('login')->with('error', 'Username dan password anda tidak valid!');
    }

    public function logout(){
        Auth::guard('penilai')->logout();
        return redirect()->route('login')->with('success', 'Terima kasih telah menggunakan sistem.');
    }

    public function ubahpass(){
        return view('ubahpassword');
    }

    public function passStore(UbahPasswordRequest $req){
        $passwordLama = $req->passwordLama;

        if(Hash::check($passwordLama, Auth::guard('penilai')->user()->password)){
            $data = Penilai::find(Auth::guard('penilai')->user()->id);
            $data->password = bcrypt($req->passwordBaru1);
            $data->save();
            Auth::guard('penilai')->logout();
            return redirect()->route('login')->with('success', 'Password berhasil diubah, silahkan login terlebih dahulu.');
        }

        return redirect()->route('login.ubahpass')->with('error', 'Password lama tidak valid!');
    }
}
