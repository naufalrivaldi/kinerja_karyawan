<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GolonganRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages(){
        return [
            'required' => 'Kolom ini tidak boleh kosong!',
            'numeric' => 'Kolom ini harus berisi data numeric'
        ];
    }
    
     public function rules()
    {
        return [
            'divisiId' => 'required|numeric',
            'nama' => 'required',
            'bonusGaji' => 'required'
        ];
    }
}
