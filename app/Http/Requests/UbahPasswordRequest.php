<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UbahPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages(){
        return [
            'required' => 'Kolom tidak boleh kosong!',
            'same' => 'Password tidak valid!'
        ];
    }
    
    public function rules()
    {
        return [
            'passwordLama' => 'required',
            'passwordBaru1' => 'required|same:passwordBaru2',
            'passwordBaru2' => 'required'
        ];
    }
}
