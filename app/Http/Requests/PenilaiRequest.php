<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenilaiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages(){
        return [
            'required' => 'Kolom tidak boleh kosong!',
            'numeric' => 'Kolom ini harus berisi nilai numeric!',
            'unique' => ':attribute sudah ada, masukkan :attribute lainnya!'
        ];
    }

    public function rules()
    {   
        $id = '';
        if(!empty($this->input('id'))){
            $id = ','.$this->input('id');
        }
        return [
            'nama' => 'required',
            'nik' => 'required|max:9|unique:penilai,nik'.$id,
            'levelId' => 'required|numeric',
            'divisiId' => 'required|numeric'
        ];
    }
}
