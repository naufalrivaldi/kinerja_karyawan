<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndikatorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages(){
        return [
            'required' => 'Kolom ini tidak boleh kosong!',
            'numeric' => 'Format numeric tidak tepat!',
        ];
    }
    
    public function rules()
    {
        return [
            'golonganId' => 'required',
            'kinerja' => 'required|array',
            'kinerja.*' => 'required',
            'bobot' => 'required|array',
            'bobot.*' => 'required',
            'target' => 'required|array',
            'target.*' => 'required'
        ];
    }
}
