<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KaryawanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages(){
        return [
            'required' => 'Kolom ini tidak boleh kosong!',
            'numeric' => 'Kolom ini harus berisi data numeric!',
            'max' => 'Data kolom melebihi maksimal!',
            'unique' => ':attribute sudah ada, masukkan :attribute lainnya!'
        ];
    }
    
     public function rules()
    {
        $id = '';
        if(!empty($this->input('id'))){
            $id = ','.$this->input('id');
        }
        return [
            'nik' => 'required|max:9|unique:karyawan,nik'.$id,
            'nama' => 'required',
            'jk' => 'required|max:1',
            'golonganId' => 'required|numeric',
            'levelId' => 'required|numeric'
        ];
    }
}
