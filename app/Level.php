<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'level';
    protected $fillable = [
        'keterangan'
    ];

    public $timestamps = false;

    // fk
    public function penilai(){
        return $this->hasMany('App\Penilai', 'levelId');
    }

    public function karyawan(){
        return $this->hasMany('App\Karyawan', 'levelId');
    }
}
