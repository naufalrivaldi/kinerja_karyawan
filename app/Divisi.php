<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    protected $table = 'divisi';
    protected $fillable = [
        'namaDivisi', 'inisial'
    ];

    public $timestamps = false;

    // fk
    public function penilai(){
        return $this->hasMany('App\Divisi', 'divisiId');
    }

    public function golongan(){
        return $this->hasMany('App\Golongan', 'divisiId');
    }
}
