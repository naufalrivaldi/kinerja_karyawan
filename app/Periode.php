<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periode extends Model
{
    protected $table = 'periode';
    protected $fillable = [
        'nama', 'tglAwal', 'tglAkhir', 'status'
    ];
    public $timestamps = false;

    // fk
    public function penilaian(){
        return $this->hasMany('App\Penilaian', 'periodeId');
    }
}
