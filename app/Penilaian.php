<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    protected $table = 'penilaian';
    protected $fillable = [
        'tglPenilaian', 'periodeId', 'penilaiId', 'karyawanId', 'status'
    ];
    public $timestamps = true;

    // fk
    public function karyawan(){
        return $this->belongsTo('App\Karyawan', 'karyawanId');
    }

    public function periode(){
        return $this->belongsTo('App\Periode', 'periodeId');
    }

    public function penilai(){
        return $this->belongsTo('App\Penilai', 'penilaiId');
    }

    public function detailPenilaian(){
        return $this->hasMany('App\DetailPenilaian', 'penilaianId');
    }

    public function penilaiApprove(){
        return $this->belongsTo('App\Penilai', 'penilaiApprove');
    }
}
