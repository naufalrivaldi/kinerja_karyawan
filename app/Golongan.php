<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Golongan extends Model
{
    protected $table = 'golongan';
    protected $fillable = [
        'nama', 'bonusGaji', 'divisiId'
    ];
    public $timestamps = false;
    
    //fk
    public function karyawan(){
        return $this->hasMany('App\Karyawan', 'golonganId');
    }

    public function  divisi(){
        return $this->belongsTo('App\Divisi', 'divisiId');
    }

    public function indikator(){
        return $this->hasMany('App\Indikator', 'golonganId');
    }
}
