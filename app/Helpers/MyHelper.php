<?php 
if (! function_exists('on_php_id')) {
    function dateFormat($date){
        $date = date('d-m-Y', strtotime($date));

        return $date;
    }

    function status($stat){
        $txt = '';
        if($stat == '1'){
            $txt = '<span class="badge badge-pill badge-success">Aktif</span>';
        }else{
            $txt = '<span class="badge badge-pill badge-danger">Nonaktif</span>';
        }

        return $txt;
    }

    function cekIndikator($data){
        $ttl = 0;
        foreach($data as $data){
            if($data->status == 1){
                $ttl += 1;
            }
        }

        return $ttl;
    }

    function setDate($date){
        $date = date('d F Y', strtotime($date));

        return $date;
    }

    // set level penilaian
    function setLevel($level){
        $level -= 1;

        return $level;
    }

    // set nilai
    function nilai($realisasi, $target, $bobot){
        $total = 0;
        $total = ($realisasi/$target) * 100;
        $total = ($total * $bobot) / 100;

        return round($total);
    }

    function setVerifikasiStatus($statusVal){
        $status = '<span class="badge badge-warning">Pending</span>';

        if($statusVal == 3){
            $status = '<span class="badge badge-success">Terverifikasi</span>';
        }

        return $status;
    }

    function cekPenilaianStatus($status){
        if($status == 3){
            return true;
        }else{
            return false;
        }
    }

    function countKaryawan($data){
        $jml = 0;

        if(!empty($data)){
            foreach($data as $data){
                if($data->status == '1'){
                    $jml += 1;
                }
            }
        }

        return $jml;
    }

    function sum($detailPenilaian){
        $total = 0;
        foreach($detailPenilaian as $row){
            $nilai = 0;
            $nilai = ($row->realisasi / $row->indikator->target) * 100;
            $nilai = (round($nilai) * $row->indikator->bobot) / 100;
            $total += $nilai;
        }

        return round($total);
    }

    function rataKinerja($penilaian){
        $total = 0;
        if(!empty($penilaian)){
            foreach($penilaian as $row){
                foreach($row->detailPenilaian as $data){
                    $nilai = 0;
                    $nilai = ($data->realisasi / $data->indikator->target) * 100;
                    $nilai = (round($nilai) * $data->indikator->bobot) / 100;
                    $total += $nilai;
                }
            }
        }

        return round($total/$penilaian->count());
    }
}