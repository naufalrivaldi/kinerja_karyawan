<?php
namespace App\Helpers;
 
use Illuminate\Support\Facades\DB;

use App\Penilaian;
use App\Periode;
use App\Karyawan;

use Hash;
use Auth;
 
class HelpPenilaian {
    /**
     * @param int $user_id User-id
     * 
     * @return string
     */

    // Set nilai untuk hasil penilaian
    public static function nilai($indikatorId, $karyawanId) {
        $realisasi = 0;
        $total = 0;
        $periode = Periode::where('status', '1')->first();
        $penilaian = Penilaian::where('karyawanId', $karyawanId)->where('periodeId', $periode->id)->first();
        
        if(isset($penilaian)){
            foreach($penilaian->detailPenilaian as $data){
                if($data->indikatorId == $indikatorId){
                    $realisasi = $data->realisasi;
                    $total = ($realisasi/$data->indikator->target) * 100;
                    $total = ($total * $data->indikator->bobot) / 100;
                }
            }
        }

        return round($total);
    }

    // hitung indikator
    public static function countIndikator($data){
        $val = 0;
        foreach($data as $data){
            if($data->status == 1){
                $val += 1;
            }
        }

        return $val;
    }

    public static function countIndikatorLaporan($data){
        $val = 0;
        foreach($data as $data){
            $val += 1;
        }

        return $val;
    }

    // cek penilaian
    public static function cekPenilaian($karyawanId, $periodeId){
        $status = 1;
        $data = Penilaian::where('periodeId', $periodeId)->where('karyawanId', $karyawanId)->first();

        if(!empty($data)){
            $status = $data->status;
        }

        if($status == 1){
            return true;
        }else{
            return false;
        }
    }

    public static function cekPenilaianStatus($karyawanId, $periodeId){
        $status = 1;
        $data = Penilaian::where('periodeId', $periodeId)->where('karyawanId', $karyawanId)->first();

        if(!empty($data)){
            $status = $data->status;
        }

        switch ($status) {
            case '1':
                return ' <span class="badge badge-warning">Proses</span>';
                break;

            case '2':
                return '<span class="badge badge-success">Data Terkirim</span>';
                break;

            case '3':
                return '<span class="badge badge-info">Verifikasi</span>';
                break;
            
            default:
                # code...
                break;
        }
    }

    // set data terikirim
    public static function setTerkirim($periodeId, $golonganId){
        $count = 0;
        $penilaian = Penilaian::where('periodeId', $periodeId)->whereHas('karyawan', function($query) use ($golonganId){
            $query->where('golonganId', $golonganId);
        })->where('status', '>', 1)->get();

        if(!empty($penilaian)){
            $count = $penilaian->count();
        }

        return $count;
    }

    // set data verifikasi
    public static function setVerifikasi($periodeId, $golonganId){
        $count = 0;
        $penilaian = Penilaian::where('periodeId', $periodeId)->whereHas('karyawan', function($query) use ($golonganId){
            $query->where('golonganId', $golonganId);
        })->where('status', 3)->get();

        if(!empty($penilaian)){
            $count = $penilaian->count();
        }

        return $count;
    }

    // cek kirim data
    public static function cekKirimData($periodeId, $golonganId){
        $arrayKaryawan = array();
        $karyawan = Karyawan::where('golonganId', $golonganId)->where('status', 1)->get();

        foreach($karyawan as $karyawan){
            array_push($arrayKaryawan, $karyawan->id);
        }

        $penilaian = Penilaian::whereHas('karyawan', function($query){
            $query->where('status', 1);
        })->where('periodeId', $periodeId)->whereIn('karyawanId', $arrayKaryawan)->get();

        if(empty($penilaian)){
            return false;
        }else{
            if(count($arrayKaryawan) == count($penilaian)){
                return true;
            }else{
                return false;
            }
        }
    }

    public static function cekKirimDataVerif($periodeId, $golonganId, $jmlData){
        $arrayKaryawan = array();
        $karyawan = Karyawan::where('golonganId', $golonganId)->get();

        foreach($karyawan as $karyawan){
            array_push($arrayKaryawan, $karyawan->id);
        }

        $penilaian = Penilaian::where('periodeId', $periodeId)->whereIn('karyawanId', $arrayKaryawan)->where('status', '>', 1)->get();

        if(empty($penilaian)){
            return false;
        }else{
            if($jmlData == count($penilaian)){
                return true;
            }else{
                return false;
            }
        }
    }

    public static function setGolongan($golonganId, $periodeId){
        $data = Penilaian::whereHas('karyawan', function($query) use ($golonganId){
            $query->where('golonganId', $golonganId);
        })->where('periodeId', $periodeId)->first();
        
        return $data->detailPenilaian;
    }

    public static function setGaji($golonganId, $periodeId){
        $data = Penilaian::whereHas('karyawan', function($query) use ($golonganId){
            $query->where('golonganId', $golonganId);
        })->where('periodeId', $periodeId)->first();
        
        return $data->karyawan->golongan->bonusGaji;
    }

    public static function defaultPass(){
        $default = '123456';

        if(Hash::check($default, Auth::guard('penilai')->user()->password)){
            return true;
        }else{
            return false;
        }
    }
}