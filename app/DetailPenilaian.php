<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPenilaian extends Model
{
    protected $table = 'detail_penilaian';
    protected $fillable = [
        'penilaianId', 'indikatorId', 'realisasi', 'keterangan'
    ];
    public $timestamps = false;

    // fk
    public function indikator(){
        return $this->belongsTo('App\Indikator', 'indikatorId');
    }

    public function penilaian(){
        return $this->belongsTo('App\Penilaian', 'penilaianId');
    }
}
