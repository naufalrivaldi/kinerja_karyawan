<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = 'karyawan';
    protected $fillable = [
        'nik', 'nama', 'jk', 'status', 'divisiId', 'jabatanId', 'levelId', 'golonganId'
    ];

    public $timestamps = false;
    
    // fk
    public function golongan(){
        return $this->belongsTo('App\Golongan', 'golonganId');
    }

    public function level(){
        return $this->belongsTo('App\Level', 'levelId');
    }

    public function penilaian(){
        return $this->hasMany('App\Penilaian', 'karyawanId');
    }
}
