<!doctype html>
<html lang="en">
 
<head>
    @include('page.header')
</head>

<body>
    <!-- main wrapper -->
    <div class="dashboard-main-wrapper">

        <!-- navbar -->
        @include('page.navbar')
        <!-- end navbar -->

        <!-- left sidebar -->
        @include('page.sidebar')
        <!-- end left sidebar -->

        <!-- wrapper  -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    @include('page.alert')
                    @include('page.alertPass')
                    @yield('content')
                </div>
            </div>
            
            <!-- footer -->
            @include('page.footer')
        </div>
    </div>
    
    <!-- Modal -->
    @yield('modal')

    <!-- Optional JavaScript -->
    @include('page.javascript')
</body>
 
</html>