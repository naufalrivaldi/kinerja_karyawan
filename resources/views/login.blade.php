<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link href="{{ asset('vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('libs/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/css/style.css') }}">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center">
                <a href=""><img class="logo-img" src="{{ asset('images/logo-cwa.png') }}" alt="logo" width="100"></a>
                <span class="splash-description">PENILAIAN KINERJA KARYAWAN<br><b>LOGIN</b></span>
            </div>
            <div class="card-body">
                @include('page.alert')
                <form method="POST" action="{{ route('login.signin') }}">
                    @csrf
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="nik" type="text" placeholder="NIK" autocomplete="off" name="nik" onkeyup="convertToMin(this);" onkeypress="return hanyaAngka(event)" maxlength="9" autofocus>

                        <!-- error -->
                        @if($errors->has('nik'))
                            <div class="text-danger">
                                {{ $errors->first('nik') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="password" type="password" placeholder="Password" name="password">

                        <!-- error -->
                        @if($errors->has('password'))
                            <div class="text-danger">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-warning btn-lg btn-block">Masuk</button>
                </form>
            </div>
            <div class="card-footer">
                <p class="text-center">Copyright © 2020 - Naufal Rivaldi. All rights reserved.</p>
            </div>
        </div>
    </div>
  
    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
    <script>
        function convertToMin(objek) {
            separator = ".";
            a = objek.value;
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 4) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + separator + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                }
            }
            objek.value = c;
        }

        function hanyaAngka(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            
            return true;
        }
    </script>
</body>
 
</html>