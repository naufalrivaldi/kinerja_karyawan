@extends('master')

@section('title', 'Periode')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Periode </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Penilaian</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ route('periode') }}" class="breadcrumb-link">Periode</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Form</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="display-7">{{ (!empty($id))? 'Edit' : 'Tambah' }} Periode</h3>
            </div>
            <div class="card-body">
                <form action="{{ (empty($id))? route('periode.store') : route('periode.update')  }}" method="POST">
                    @csrf
                    @if(!empty($id))
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <div class="row justify-content-md-center">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Nama Periode</label>
                                <input type="text" name="nama" class="form-control" id="nama" value="{{ (!empty($id))? $periode->nama : '' }}">

                                <!-- error -->
                                @if($errors->has('nama'))
                                    <div class="text-danger">
                                        {{ $errors->first('nama') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="nama">Tanggal Awal</label>
                                <input type="date" name="tglAwal" class="form-control" id="tglAwal" value="{{ (!empty($id))? $periode->tglAwal : '' }}">

                                <!-- error -->
                                @if($errors->has('tglAwal'))
                                    <div class="text-danger">
                                        {{ $errors->first('tglAwal') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="nama">Tanggal Akhir</label>
                                <input type="date" name="tglAkhir" class="form-control" id="tglAkhir" value="{{ (!empty($id))? $periode->tglAkhir : '' }}">

                                <!-- error -->
                                @if($errors->has('tglAkhir'))
                                    <div class="text-danger">
                                        {{ $errors->first('tglAkhir') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkStatus" name="status" value="1" {{ (!empty($id)) ? ($periode->status == '1') ? 'checked' : '' : '' }}>
                                <label class="form-check-label" for="checkStatus">Langsung Aktifkan?</label>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('#tglAwal').on('change', function(){
            var tglAwal = $(this).val();

            $('#tglAkhir').attr('min', tglAwal);
        });
    });
</script>
@endsection