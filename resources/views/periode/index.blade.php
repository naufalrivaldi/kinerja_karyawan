@extends('master')

@section('title', 'Periode')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Periode </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Data</li>
                        <li class="breadcrumb-item active" aria-current="page">Periode</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('periode.form') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Tambah Periode</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>NAMA</th>
                                <th>TANGGAL AWAL</th>
                                <th>TANGGAL AKHIR</th>
                                <th>STATUS</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($periode as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <td>{{ dateFormat($row->tglAwal) }}</td>
                                    <td>{{ dateFormat($row->tglAkhir) }}</td>
                                    <td>{!! status($row->status) !!}</td>
                                    <td>
                                        @if($row->status == '1')
                                            <a href="{{ route('periode.nonaktif', ['id' => $row->id]) }}" class="btn btn-sm btn-danger"><i class="fas fa-times-circle"></i></a>
                                        @else
                                            <a href="{{ route('periode.aktif', ['id' => $row->id]) }}" class="btn btn-sm btn-success"><i class="fas fa-check-circle"></i></a>
                                        @endif
                                        
                                        <a href="{{ route('periode.edit', ['id' => $row->id]) }}" class="btn btn-sm btn-success"><i class="fas fa-cog"></i></a>

                                        @if($row->status == '2')
                                            <button class="btn btn-sm btn-danger btn-delete" data-id="{{ $row->id }}"><i class="far fa-trash-alt"></i></button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('.btn-delete').on('click', function(){
            var id = $(this).data('id');
            swal({
                title: "Hapus Data?",
                text: "Data yang dihapus tidak dapat dikembalikan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'POST',
                        data: {
                            '_token': '{{ csrf_token() }}',
                            'id': id
                        },
                        url: "{{ route('periode.destroy') }}",
                        success: function(data){
                            location.reload();
                        }
                    });
                }
            });
        });
    });
</script>
@endsection