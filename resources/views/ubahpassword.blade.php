@extends('master')

@section('title', 'Dashboard')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Ubah Password </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Ubah Password</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('login.ubahpass.store') }}" method="POST">
                    @csrf 
                    <div class="row justify-content-md-center">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="passwordLama">Password Lama</label>
                                <input type="password" name="passwordLama" class="form-control" id="passwordLama">

                                <!-- error -->
                                @if($errors->has('passwordLama'))
                                    <div class="text-danger">
                                        {{ $errors->first('passwordLama') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="passwordBaru1">Password Baru</label>
                                <input type="password" name="passwordBaru1" class="form-control" id="passwordBaru1">

                                <!-- error -->
                                @if($errors->has('passwordBaru2'))
                                    <div class="text-danger">
                                        {{ $errors->first('passwordBaru2') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="passwordBaru2">Konfirmasi Password Baru</label>
                                <input type="password" name="passwordBaru2" class="form-control" id="passwordBaru2">

                                <!-- error -->
                                @if($errors->has('passwordBaru1'))
                                    <div class="text-danger">
                                        {{ $errors->first('passwordBaru1') }}
                                    </div>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">Ubah Password</button>
                        </div>  
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>
@endsection