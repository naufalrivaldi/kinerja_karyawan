@extends('master')

@section('title', 'Verifikasi Penilaian')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Verifikasi Penilaian </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Penilaian</li>
                        <li class="breadcrumb-item active" aria-current="page">Verifikasi Penilaian</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form>
                    <div class="row">
                        <div class="col-md-3">
                            <select name="periodeId" id="periodeId" class="form-control" required>
                                <option value="">Pilih</option>
                                @foreach($periode as $periode)
                                    <option value="{{ $periode->id }}">{{ $periode->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <input type="submit" class="btn btn-primary btn-sm" value="Cari">
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-body">
                <h1>{{ $setPeriode->nama }}</h1>
                <p>
                    Tanggal Periode : <span class="badge badge-success">{{ setDate($setPeriode->tglAwal).' s/d '.setDate($setPeriode->tglAkhir) }}</span>
                </p>
                <div class="table-responsive">
                    <table class="table dataTable table-striped">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>NAMA GOLONGAN</th>
                                <th>JML KARYAWAN</th>
                                <th>DATA TERIKIRIM</th>
                                <th>DATA TERVERIFIKASI</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($golongan as $golongan)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $golongan->nama }}</td>
                                    <td>{{ countKaryawan($golongan->karyawan) }} Orang</td>
                                    <td>{{ HelpPenilaian::setTerkirim($setPeriode->id, $golongan->id) }}</td>
                                    <td>{{ HelpPenilaian::setVerifikasi($setPeriode->id, $golongan->id) }}</td>
                                    <td>
                                        <a href="{{ route('verifikasi.view', array('id' => $golongan->id, 'periodeId' => $setPeriode->id)) }}" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>   
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="modalIndikator" tabindex="-1" role="dialog" aria-labelledby="modalIndikatorTitle" aria-hidden="true">
    <div class="modal-dialog modalWidthLg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title display-7" id="modalIndikatorTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="view"></div>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('document').ready(function(){
        $('.btn-modal').on('click', function(){
            var divisiId = $(this).data('id');
            var title = $(this).data('title');
            $.ajax({
                type: "GET",
                url: "{{ route('indikator.view') }}",
                data: "id="+divisiId,
                success: function(data){
                    $('#modalIndikatorTitle').empty();
                    $('#modalIndikatorTitle').append(title);
                    $('#view').empty();
                    $('#view').append(data);
                }
            });
        });
    });

    $(document).on('click', '.btn-delete', function(){
        var id = $(this).data('id');
        swal({
            title: "Hapus Indikator?",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'GET',
                    data: {
                        'id': id
                    },
                    url: "{{ route('indikator.nonaktif') }}",
                    success: function(data){
                        location.reload();
                    }
                });
            }
        });
    });
</script>
@endsection