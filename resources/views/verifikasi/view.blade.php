@extends('master')

@section('title', 'Verifikasi Penilaian')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Verifikasi Penilaian </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Penilaian</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ route('verifikasi') }}" class="breadcrumb-link">Verifikasi Penilaian</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Data Penilaian Karyawan</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h1>Detail Verifikasi Penilaian</h1>
            </div>
            <div class="card-header">
                <h1>{{ $setPeriode->nama }}</h1>
                <p>
                    Tanggal Periode : <span class="badge badge-success">{{ setDate($setPeriode->tglAwal).' s/d '.setDate($setPeriode->tglAkhir) }}</span>
                </p>
                <h3 style="margin-bottom: 0">Nama Golongan : {{ $golongan->nama }}</h3>
            </div>
            <div class="card-header menu menuHidden">
                <button class="btn btn-success btn-sm btn-verifikasi">Verifikasi</button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table dataTable table-striped">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="checkAll">
                                </th>
                                <th>NO</th>
                                <th>NAMA KARYAWAN</th>
                                <th>STATUS</th>
                                <th> AKSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($penilaian as $penilaian)
                                <tr>
                                    <td>
                                        @if($penilaian->status < 3 && Auth::guard('penilai')->user()->levelId != 5)
                                            <input type="checkbox" class="checkData" value="{{ $penilaian->id }}" name="penilaianId">
                                        @endif
                                    </td>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $penilaian->karyawan->nama }}</td>
                                    <td>{!! setVerifikasiStatus($penilaian->status) !!}</td>
                                    <td>
                                        <button class="btn btn-info btn-sm btn-modal" data-toggle="modal" data-target="{{ (cekPenilaianStatus($penilaian->status))? '#modalPenilaianAcc' : '#modalPenilaian' }}" data-penilaianid="{{ $penilaian->id }}"><i class="fas fa-eye"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="modalPenilaian" tabindex="-1" role="dialog" aria-labelledby="modalPenilaianTitle" aria-hidden="true">
    <div class="modal-dialog modalWidthLg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title display-7" id="modalPenilaianTitle">Data Penilaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="view"></div>      
            </div>
            <div class="modal-footer">
                @if(Auth::guard('penilai')->user()->levelId != 5)
                <button type="button" class="btn btn-success verifikasi" data-penilaianid="">Verifikasi</button>
                @endif
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPenilaianAcc" tabindex="-1" role="dialog" aria-labelledby="modalPenilaianAccTitle" aria-hidden="true">
    <div class="modal-dialog modalWidthLg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title display-7" id="modalPenilaianAccTitle">Data Penilaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="view"></div>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('document').ready(function(){
        // modal view
        $('.btn-modal').on('click', function(){
            var penilaianId = $(this).data('penilaianid');
            $.ajax({
                type: "GET",
                url: "{{ route('verifikasi.view.penilaian') }}",
                data: {
                    'penilaianId' : penilaianId
                },
                success: function(data){
                    $('.view').empty();
                    $('.view').append(data);

                    var penilaianId = $('.getId').data('penilaian');
                    $('.verifikasi').data('penilaianid', penilaianId);
                }
            });
        });

        // checked
        $('.checkAll').on('click', function(){
            if($(this).is(':checked')){
                $('.checkData').prop('checked', true);
                $('.menu').removeClass('menuHidden');
            }else{
                $('.checkData').prop('checked', false);
                $('.menu').addClass('menuHidden');
            }
            
            cekButtonVerif()
        });

        $('.checkData').on('click', function(){
            cekButtonVerif()
        });

        function cekButtonVerif(){
            if($('.checkData').is(':checked')){
                $('.menu').removeClass('menuHidden');
            }else{
                $('.menu').addClass('menuHidden');
            }
        }
    });

    $(document).on('click', '.verifikasi', function(){
        var id = $(this).data('penilaianid');
        swal({
            title: "Verifikasi Penilaian",
            text: "Form akan diteruskan kepada HRD.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'POST',
                    data: {
                        'penilaianId': id,
                        '_token': '{{ csrf_token() }}',
                    },
                    url: "{{ route('verifikasi.acc') }}",
                    success: function(data){
                        location.reload();
                    }
                });
            }
        });
    });

    // klik btn verifikasi
    $(document).on('click', '.btn-verifikasi', function(){
        var array = [];
        $('input[name=penilaianId]:checked').each(function(){
            array.push($(this).val());
        })

        swal({
            title: "Verifikasi Penilaian",
            text: "Form akan diteruskan kepada HRD.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'POST',
                    data: {
                        'penilaianId': array,
                        '_token': '{{ csrf_token() }}',
                    },
                    url: "{{ route('verifikasi.acc.array') }}",
                    success: function(data){
                        location.reload();
                    }
                });
            }
        });
    });
</script>
@endsection