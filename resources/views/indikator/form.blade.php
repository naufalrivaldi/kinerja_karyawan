@extends('master')

@section('title', 'Indikator')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Form Indikator </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Data</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ route('indikator') }}" class="breadcrumb-link">Indikator</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Form</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="display-7">{{ (!empty($id))? 'Edit' : 'Tambah' }} Indikator</h3>
            </div>
            <div class="card-body">
                <form action="{{ (empty($id))? route('indikator.store') : route('indikator.update')  }}" method="POST">
                    @csrf
                    @if(!empty($id))
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $id }}">
                        <input type="hidden" class="golonganId" value="{{ $golongan->id }}">
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <h3>{{ $golongan->divisi->namaDivisi }}</h3>
                            <div class="form-group">
                                <label for="golongan">Golongan</label>
                                <select name="golonganId" id="golongan" class="form-control" readonly>
                                    <option value="{{ $golongan->id }}">{{ $golongan->nama }}</option>
                                </select>
                                <!-- error -->
                                @if($errors->has('golonganId'))
                                    <div class="text-danger">
                                        {{ $errors->first('golonganId') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- indikator -->
                    <hr>
                    <h2>Indikator :</h2>
                    <h3>Total Bobot : <span class="ttlBobot"></span></h3>
                    <p class="text-danger bobotAlert"></p>
                    <div id="form-plus">
                        <?php $no = 1; ?>
                        @if(!empty($id))
                            @if(cekIndikator($golongan->indikator) > 0)
                                @foreach($golongan->indikator as $data)
                                    @if($data->status == 1)
                                        <div id="row{{ ($no > 1)? $no : '' }}">
                                            <input type="hidden" name="indikatorId[]" value="{{ $data->id }}">
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <label class="">Kinerja</label>
                                                    <input type="text" name="kinerja[]" class="form-control" value="{{ (!empty($id))? $data->kinerja : '' }}">
                                                    
                                                    @if($errors->has('kinerja'))
                                                        <div class="text-danger">
                                                            {{ $errors->first('kinerja') }}
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="">Bobot (%)</label>
                                                    <input type="number" name="bobot[]" class="form-control bobot" value="{{ (!empty($id))? $data->bobot : '' }}">

                                                    @if($errors->has('bobot'))
                                                        <div class="text-danger">
                                                            {{ $errors->first('bobot') }}
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    <label class="">Target</label>
                                                    <input type="number" name="target[]" class="form-control" value="{{ (!empty($id))? $data->target : '' }}">

                                                    @if($errors->has('target'))
                                                        <div class="text-danger">
                                                            {{ $errors->first('target') }}
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="form-group col-sm-2">
                                                    @if($no == 1)
                                                        <?php $no++; ?>
                                                        <a href="#" id="plus" class="btn btn-success btn-sm" style="margin-top: 28px">+</a>
                                                    @else
                                                        <a href="#" class="btn btn-danger remove" style="margin-top: 28px" id="{{$no++}}">-</a>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <label class="">Detail Target</label>
                                                    <textarea name="detailTarget[]" class="form-control" rows="5">{{ (!empty($id))? $data->detailTarget : '' }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @else
                                <div id="row{{ ($no > 1)? $no : '' }}">
                                    <div class="row">
                                        <div class="form-group col-sm-4">
                                            <label class="">Kinerja</label>
                                            <input type="text" name="kinerja[]" class="form-control" value="">
                                            
                                            @if($errors->has('kinerja'))
                                                <div class="text-danger">
                                                    {{ $errors->first('kinerja') }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-sm-2">
                                            <label class="">Bobot (%)</label>
                                            <input type="number" name="bobot[]" class="form-control bobot" value="">

                                            @if($errors->has('bobot'))
                                                <div class="text-danger">
                                                    {{ $errors->first('bobot') }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-sm-2">
                                            <label class="">Target</label>
                                            <input type="number" name="target[]" class="form-control" value="">

                                            @if($errors->has('target'))
                                                <div class="text-danger">
                                                    {{ $errors->first('target') }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-sm-2">
                                            @if($no == 1)
                                                <?php $no++; ?>
                                                <a href="#" id="plus" class="btn btn-success btn-sm" style="margin-top: 28px">+</a>
                                            @else
                                                <a href="#" class="btn btn-danger remove" style="margin-top: 28px" id="{{$no++}}">-</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <label class="">Detail Target</label>
                                            <textarea name="detailTarget[]" class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @else
                        <div id="row{{ ($no > 1)? $no : '' }}">
                            <div class="row">
                                <div class="form-group col-sm-4">
                                    <label class="">Kinerja</label>
                                    <input type="text" name="kinerja[]" class="form-control" value="">
                                    
                                    @if($errors->has('kinerja'))
                                        <div class="text-danger">
                                            {{ $errors->first('kinerja') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group col-sm-2">
                                    <label class="">Bobot (%)</label>
                                    <input type="number" name="bobot[]" class="form-control bobot" value="">

                                    @if($errors->has('bobot'))
                                        <div class="text-danger">
                                            {{ $errors->first('bobot') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group col-sm-2">
                                    <label class="">Target</label>
                                    <input type="number" name="target[]" class="form-control" value="">

                                    @if($errors->has('target'))
                                        <div class="text-danger">
                                            {{ $errors->first('target') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group col-sm-2">
                                    @if($no == 1)
                                        <?php $no++; ?>
                                        <a href="#" id="plus" class="btn btn-success btn-sm" style="margin-top: 28px">+</a>
                                    @else
                                        <a href="#" class="btn btn-danger remove" style="margin-top: 28px" id="{{$no++}}">-</a>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <label class="">Detail Target</label>
                                    <textarea name="detailTarget[]" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary btn-submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        
        // klik divisi
        $('#divisi').on('change', function(){
            var divisiId = $(this).val();
            $.ajax({
                type: 'GET',
                data: "id="+divisiId,
                url: "{{ route('karyawan.fillGolongan') }}",
                success: function(data){
                    $('#golongan').removeAttr('disabled');
                    $('.text-small').empty();
                    $('.fillGolongan').empty();
                    $('.fillGolongan').append(data);
                }
            });
        });

        var divisiId = '';
        var golonganId = '';
        // edit selected
        @if(!empty($id))
            $('#divisi option').each(function() {
                if($(this).is(':selected')){
                    divisiId = $(this).val();
                }
            });

            golonganId = $('.golonganId').val();
            $.ajax({
                type: 'GET',
                data: {
                    'divisiId' : divisiId,
                    'golonganId' : golonganId,
                },
                url: "{{ route('karyawan.fillGolonganEdit') }}",
                success: function(data){
                    $('.text-small').empty();
                    $('.fillGolongan').empty();
                    $('.fillGolongan').append(data);
                }
            });
        @endif

        // click plus
        var i = 1;
        $('#plus').click(function (e) {
            e.preventDefault();
            $('#form-plus').append('<div id="row'+i+'"><div class="row"><div class="form-group col-sm-4"><label class="">Kinerja</label><input type="text" name="kinerja[]" class="form-control"></div><div class="form-group col-sm-2"><label class="">Bobot (%)</label><input type="number" name="bobot[]" class="form-control bobot"></div><div class="form-group col-sm-2"><label class="">Target</label><input type="number" name="target[]" class="form-control"></div><div class="form-group col-sm-2"><a href="#" class="btn btn-danger remove" style="margin-top: 28px" id="'+i+'">-</a></div></div><div class="row"><div class="form-group col-sm-12"><label class="">Detail Target</label><textarea name="detailTarget[]" class="form-control" rows="5"></textarea></div></div></div>');

            i++;

            cekBobot();
        });

        $(document).on('click', '.remove', function(e){
            e.preventDefault();
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();

            cekBobot();
        });

        // bobot   
        $('.btn-submit').attr('disabled', 'disabled');     
        $(document).on('keyup', '.bobot', function(){
            cekBobot();
        });

        // jika id ada dan total bobot ada
        @if(!empty($id))
            $('.btn-submit').removeAttr('disabled');
            cekBobot();
        @endif

        // function
        function cekBobot(){
            var ttl = 0;
            var value = $('.bobot').map(function(){return $(this).val();}).get();

            for(let i=0; i<value.length; i++){
                if(value[i] === ''){
                    ttl += 0;    
                }else{
                    ttl += parseFloat(value[i]);
                }
            }
            
            $('.ttlBobot').empty();
            $('.ttlBobot').append(ttl);

            if(ttl > 100){
                $('.bobotAlert').empty();
                $('.bobotAlert').append("Bobot Melebihi Batas Maksimal!");
                $('#plus').addClass('disabled');
                $('.btn-submit').attr('disabled', 'disabled');
                $('.bobot').addClass('is-invalid');
            }else if(ttl == 100){
                $('.btn-submit').removeAttr('disabled');
                $('.bobotAlert').empty();
                $('#plus').removeClass('disabled');
                $('.bobot').removeClass('is-invalid');
            }else{
                $('.bobotAlert').empty();
                $('#plus').removeClass('disabled');
                $('.bobot').removeClass('is-invalid');
                $('.btn-submit').attr('disabled', 'disabled');
            }
        }
    });
</script>
@endsection