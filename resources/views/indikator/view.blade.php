<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        @foreach($golongan as $golhead)
            <a class="nav-item nav-link {{ ($no == 1) ? 'active' : '' }}" id="nav-home-tab" data-toggle="tab" href="#nav{{ $no }}" role="tab" aria-selected="{{ ($no == 1) ? 'true' : '' }}" aria-controls="nav{{ $no++ }}">{{ $golhead->nama }}</a>
        @endforeach
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <?php $no = 1 ?>
    @foreach($golongan as $row)
        <div class="tab-pane fade show {{ ($no == 1) ? 'active' : '' }}" id="nav{{ $no }}" role="tabpanel" aria-labelledby="nav{{ $no++ }}-tab">
            <?php $noTable = 1; $ttlBobot = 0; ?>
            @if($row->indikator->count() > 0)
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('indikator.edit', ['id' => $row->id]) }}" class="btn btn-sm btn-success"><i class="fas fa-cog"></i></a> 
                        <button class="btn btn-sm btn-danger btn-delete" data-id="{{ $row->id }}"><i class="far fa-trash-alt"></i></button>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped dataTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Indikator</th>
                                    <th>Bobot</th>
                                    <th>Target</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($row->indikator as $data)
                                    @if($data->status == 1)
                                        <?php $ttlBobot += $data->bobot; ?>
                                        <tr>
                                            <td>{{ $noTable++ }}</td>
                                            <td>{{ $data->kinerja }}</td>
                                            <td>{{ $data->bobot }} %</td>
                                            <td>{{ $data->target }}</td>
                                            <td>{{ $data->detailTarget }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                @if($ttlBobot != 0)
                                    <tr>
                                        <td colspan="2" align="center"><b>Total</b></td>
                                        <td>{{ $ttlBobot }} %</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <a href="{{ route('indikator.form', ['id' => $row->id]) }}" class="btn btn-primary btn-sm mt-3"><i class="fas fa-plus"></i> Tambah Indikator</a>
                <p class="mt-2 text-center">Data tidak ditemukan.</p>
            @endif
        </div>
    @endforeach
</div>