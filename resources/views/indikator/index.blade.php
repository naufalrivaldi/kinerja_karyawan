@extends('master')

@section('title', 'Indikator')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Indikator </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Data</li>
                        <li class="breadcrumb-item active" aria-current="page">Indikator</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>INISIAL</th>
                                <th>NAMA DIVISI</th>
                                <th>JUMLAH GOLONGAN</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($divisi as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $row->inisial }}</td>
                                    <td>{{ $row->namaDivisi }}</td>
                                    <td>{{ $row->golongan->count() }}</td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-sm btn-modal" data-toggle="modal" data-target="#modalIndikator" data-id="{{ $row->id }}" data-title="{{ $row->namaDivisi }}">
                                        <i class="fas fa-eye"></i> Lihat
                                    </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="modalIndikator" tabindex="-1" role="dialog" aria-labelledby="modalIndikatorTitle" aria-hidden="true">
    <div class="modal-dialog modalWidthLg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title display-7" id="modalIndikatorTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="view"></div>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).on('click', '.btn-modal', function(){
        var divisiId = $(this).data('id');
        var title = $(this).data('title');
        $.ajax({
            type: "GET",
            url: "{{ route('indikator.view') }}",
            data: "id="+divisiId,
            success: function(data){
                $('#modalIndikatorTitle').empty();
                $('#modalIndikatorTitle').append(title);
                $('#view').empty();
                $('#view').append(data);
            }
        });
    });

    $(document).on('click', '.btn-delete', function(){
        var id = $(this).data('id');
        swal({
            title: "Hapus Indikator?",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'GET',
                    data: {
                        'id': id
                    },
                    url: "{{ route('indikator.nonaktif') }}",
                    success: function(data){
                        location.reload();
                    }
                });
            }
        });
    });
</script>
@endsection