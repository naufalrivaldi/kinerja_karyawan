@extends('master')

@section('title', 'Golongan')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Golongan </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Data</li>
                        <li class="breadcrumb-item active" aria-current="page">Golongan</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card"> 
            <div class="card-header">
                <a href="{{ route('golongan') }}" class="btn btn-success btn-sm"><i class="far fa-arrow-alt-circle-left"></i> Kembali</a>
                <a href="{{ route('golongan.form') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Tambah Golongan</a>
            </div>
            <div class="card-header">
                <h3 class="display-5">{{ $divisi->namaDivisi }}</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>NAMA</th>
                                <th>BONUS GAJI</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($golongan as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <td>Rp. {{ number_format($row->bonusGaji) }}</td>
                                    <td>
                                        <a href="{{ route('golongan.edit', ['id' => $row->id]) }}" class="btn btn-sm btn-success"><i class="fas fa-cog"></i></a>
                                        <button class="btn btn-sm btn-danger btn-delete" data-id="{{ $row->id }}"><i class="far fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('.btn-delete').on('click', function(){
            var id = $(this).data('id');
            swal({
                title: "Hapus Data?",
                text: "Data yang dihapus tidak dapat dikembalikan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'POST',
                        data: {
                            '_token': '{{ csrf_token() }}',
                            'id': id
                        },
                        url: "{{ route('golongan.destroy') }}",
                        success: function(data){
                            location.reload();
                        }
                    });
                }
            });
        });
    });
</script>
@endsection