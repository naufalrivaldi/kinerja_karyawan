@extends('master')

@section('title', 'Golongan')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Form Golongan </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Data</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ route('golongan') }}" class="breadcrumb-link">Golongan</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Form</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="display-7">{{ (!empty($id))? 'Edit' : 'Tambah' }} Golongan</h3>
            </div>
            <div class="card-body">
                <form action="{{ (empty($id))? route('golongan.store') : route('golongan.update')  }}" method="POST">
                    @csrf
                    @if(!empty($id))
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <div class="row justify-content-md-center">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="divisi">Divisi</label>
                                <select name="divisiId" id="divisi" class="form-control">
                                    <option value="">Pilih</option>
                                    @foreach($divisi as $divisi)
                                        <option value="{{ $divisi->id }}" {{ (!empty($id))? ($golongan->divisiId == $divisi->id)? 'selected' : '' : '' }}>{{ $divisi->inisial.' - '.$divisi->namaDivisi }}</option>
                                    @endforeach
                                </select>

                                <!-- error -->
                                @if($errors->has('divisiId'))
                                    <div class="text-danger">
                                        {{ $errors->first('divisiId') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama</label>
                                <input type="text" name="nama" class="form-control" id="nama" value="{{ (!empty($id))? $golongan->nama : '' }}">

                                <!-- error -->
                                @if($errors->has('nama'))
                                    <div class="text-danger">
                                        {{ $errors->first('nama') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="bonusGaji">Bonus Gaji</label>
                                <input type="number" name="bonusGaji" class="form-control" id="bonusGaji" value="{{ (!empty($id))? $golongan->bonusGaji : '' }}">

                                <!-- error -->
                                @if($errors->has('bonusGaji'))
                                    <div class="text-danger">
                                        {{ $errors->first('bonusGaji') }}
                                    </div>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection