@extends('master')

@section('title', 'Penilaian')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Penilaian </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Penilaian</li>
                        <li class="breadcrumb-item active" aria-current="page">Penilaian Karyawan</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="display-5">Periode Penilaian</h3>
                @if(!empty($periode))
                    <table class="table">
                        <tr>
                            <td width="10%">Periode</td>
                            <td width="1%">:</td>
                            <td>{{ $periode->nama }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>:</td>
                            <td>
                                <span class="badge badge-info">{{ setDate($periode->tglAwal).' s/d '.setDate($periode->tglAkhir) }}</span>
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <!-- alert periode -->
                    @if($periode->tglAwal > $dateNow)
                        <div class="alert alert-info" role="alert">
                            Belum bisa menilai dikarenakan belum memasuki periode.
                        </div>
                    @elseif($periode->tglAkhir < $dateNow)
                        <div class="alert alert-danger" role="alert">
                            Anda sudah tidak bisa menilai dikarenakan periode sudah berakhir.
                        </div>
                    @endif
                @else
                    <div class="alert alert-danger" role="alert">
                        Periode Belum ditentukan!
                    </div>
                @endif
            </div>

            <!-- jika periode tidak kosong -->
            @if(!empty($periode))
            <div class="card-body">
                <h3>Data Penilaian</h3>
                <hr>
                <!-- Tab Pane Golongan -->
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        @foreach($golongan as $golhead)
                            <a class="nav-item nav-link {{ ($no == 1) ? 'active' : '' }}" id="nav-home-tab" data-toggle="tab" href="#nav{{ $no }}" role="tab" aria-selected="{{ ($no == 1) ? 'true' : '' }}" aria-controls="nav{{ $no++ }}">{{ $golhead->golongan->nama }}</a>
                        @endforeach
                    </div>
                </nav>
                
                <div class="tab-content" id="nav-tabContent">
                    <?php $noNav = 1 ?>
                    @foreach($golongan as $row)
                        <?php $noTable = 1; $jmlData = 0; ?>
                        <div class="tab-pane fade show {{ ($noNav == 1) ? 'active' : '' }}" id="nav{{ $noNav }}" role="tabpanel" aria-labelledby="nav{{ $noNav++ }}-tab">
                            <br>

                            <!-- table penilaian -->
                            <div class="table-responsive">
                                <table class="table table-bordered dataTable mt-2 table-striped" width="100%">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" class="vCell">NO</th>
                                            <th rowspan="2" class="vCell">NAMA KARYAWAN</th>
                                            <th colspan="{{ HelpPenilaian::countIndikator($row->golongan->indikator) }}" class="cCell">INDIKATOR</th>
                                            <th rowspan="2" class="vCell text-center">TOTAL<br>(%)</th>
                                            <th rowspan="2" class="vCell text-center">STATUS</th>
                                            <th rowspan="2" class="vCell text-center">AKSI</th>
                                        </tr>
                                        <tr>
                                            <?php $idx = 1; ?>
                                            @foreach($row->golongan->indikator as $ind)
                                                @if($ind->status == '1')
                                                    <th class="text-center">Ind{{ $idx++ }}<br>{{ '('.$ind->bobot.'%)' }}</th>
                                                @endif
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($karyawan as $kr)
                                            <?php
                                                $total = 0;
                                            ?>
                                            @if($row->golonganId == $kr->golonganId)
                                                <?php $jmlData += 1; ?>
                                                <tr>
                                                    <td>{{ $noTable++ }}</td>
                                                    <td>{{ $kr->nama }}</td>
                                                    @foreach($row->golongan->indikator as $ind)
                                                        @if($ind->status == '1')
                                                        <?php
                                                            $total += HelpPenilaian::nilai($ind->id, $kr->id);
                                                        ?>
                                                            <td align="right">{{ HelpPenilaian::nilai($ind->id, $kr->id) }}</td>
                                                        @endif
                                                    @endforeach
                                                    <td align="right">{{ $total }}</td>
                                                    <td>
                                                        {!! HelpPenilaian::cekPenilaianStatus($kr->id, $periode->id) !!}
                                                    </td>
                                                    <td>
                                                        <!-- tombol penilaian -->
                                                        @if($periode->tglAwal <= $dateNow && $periode->tglAkhir >= $dateNow && Auth::guard('penilai')->user()->levelId != 5)
                                                            <!-- jika dia sudah memiliki nilai total -->
                                                            @if($total == 0)
                                                                <a href="{{ route('penilaian.form', ['karyawanId' => $kr->id]) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Nilai</a>
                                                            @else
                                                                <!-- view -->
                                                                <button class="btn btn-info btn-sm btn-modal" data-toggle="modal" data-target="#modalPenilaian" data-karyawanid="{{ $kr->id }}" data-periodeid="{{ $periode->id }}"><i class="fas fa-eye"></i></button>
                                                                <!-- edit -->
                                                                @if(HelpPenilaian::cekPenilaian($kr->id, $periode->id))
                                                                    <a href="{{ route('penilaian.edit', ['karyawanId' => $kr->id]) }}" class="btn btn-success btn-sm"><i class="fas fa-edit"></i> Edit</a>
                                                                @endif
                                                            @endif
                                                        @else
                                                            <button class="btn btn-info btn-sm btn-modal" data-toggle="modal" data-target="#modalPenilaian" data-karyawanid="{{ $kr->id }}" data-periodeid="{{ $periode->id }}"><i class="fas fa-eye"></i></button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- table penilaian -->
                            <hr>
                            <h5>Indikator :</h5>
                            <?php $no = 1; ?>
                            @foreach($row->golongan->indikator as $ind)
                                @if($ind->status == '1')
                                    <table width="100%">
                                        <tr>
                                            <td width="10%">Ind{{ $no++ }}</td>
                                            <td width="2%">:</td>
                                            <td>{{ $ind->kinerja.' ('.$ind->detailTarget.')' }}</td>
                                        </tr>
                                    </table>
                                @endif
                            @endforeach
                            <hr>
                            @if(Auth::guard('penilai')->user()->levelId != 5)
                                @if(HelpPenilaian::cekKirimData($periode->id, $row->golongan->id))
                                    @if(HelpPenilaian::cekKirimDataVerif($periode->id, $row->golongan->id, $jmlData))
                                        <p class="text-success">*Data sudah terkirim.</p>
                                    @else
                                        <a href="{{ route('penilaian.kirim', ['periodeId' => $periode->id, 'golonganId' => $row->golongan->id]) }}" class="btn btn-primary btn-sm"><i class="fas fa-paper-plane"></i> Kirim Data</a>
                                    @endif
                                @else
                                    <p class="text-danger">*Lengkapi penilaian terlebih dahulu!</p>
                                    <a href="#" class="btn btn-primary btn-sm disabled"><i class="fas fa-paper-plane"></i> Kirim Data</a>
                                @endif
                            @endif
                        </div>
                    @endforeach
                </div>
                <!-- Tab Pane Golongan -->
            </div>
            @endif

        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="modalPenilaian" tabindex="-1" role="dialog" aria-labelledby="modalPenilaian" aria-hidden="true">
    <div class="modal-dialog modalWidthLg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title display-7" id="modalPenilaianTitle">Detail Penilaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="view"></div>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).on('click', '.btn-modal', function(){
        var karyawanId = $(this).data('karyawanid');
        var periodeId = $(this).data('periodeid');

        $('.view').empty();
        $.ajax({
            type: "GET",
            url: "{{ route('penilaian.view') }}",
            data: {
                'karyawanId' : karyawanId,
                'periodeId' : periodeId
            },
            success: function(data){
                $('.view').append(data);
            }
        });
    });

    $(document).on('click', '.btn-delete', function(){
        var id = $(this).data('id');
        swal({
            title: "Hapus Indikator?",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'GET',
                    data: {
                        'id': id
                    },
                    url: "{{ route('indikator.nonaktif') }}",
                    success: function(data){
                        location.reload();
                    }
                });
            }
        });
    });
</script>
@endsection