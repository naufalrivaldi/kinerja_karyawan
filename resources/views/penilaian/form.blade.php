@extends('master')

@section('title', 'Penilaian')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Penilaian </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Penilaian</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ route('penilaian') }}" class="breadcrumb-link">Penilaian Karyawan</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Form</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="display-7">{{ (!empty($id))? 'Edit' : 'Form' }} Penilaian</h3>
                <h4>{{ $karyawan->golongan->divisi->namaDivisi.' | '.$karyawan->golongan->nama }}</h4>
            </div>
            <div class="card-body">
                <form action="{{ (empty($id))? route('penilaian.store') : route('penilaian.update')  }}" method="POST">
                    @csrf
                    @if(!empty($id))
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="karyawanId" value="{{ $karyawan->id }}">
                    <div class="row justify-content-md-center">
                        <div class="col-md-12">
                            <table width="100%">
                                <tr>
                                    <td width="10%">NIK</td>
                                    <td width="1%">:</td>
                                    <td>{{ $karyawan->nik }}</td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>{{ $karyawan->nama }}</td>
                                </tr>
                            </table>
                            <hr>
                            <h4>Indikator Penilaian :</h4>
                            @if(empty($id))
                                @foreach($karyawan->golongan->indikator as $indikator)
                                    @if($indikator->status == '1')
                                        <div class="form-group">
                                            <label for="kinerja">
                                                {{ $indikator->kinerja }} <br>
                                            </label>
                                            <input type="hidden" name="indikatorId[]" value="{{ $indikator->id }}">
                                            <input type="number" name="realisasi[]" class="form-control realisasi col-md-6" id="realisasi" value="" max="{{ $indikator->target }}" min="0" required>
                                            <p class="text-mini"><span class="text-danger">Bobot Nilai: {{ $indikator->bobot }} %</span>, <b>Target maksimal pencapaian : {{ $indikator->target }}</b><br>{{ $indikator->detailTarget }}</p>

                                            <!-- error -->
                                            @if($errors->has('realisasi.*'))
                                                <div class="text-danger">
                                                    {{ $errors->first('realisasi.*') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="keterangan">
                                                Keterangan
                                            </label>

                                            <textarea name="keterangan[]" id="keterangan" rows="5" class="form-control"></textarea>

                                            <!-- error -->
                                            @if($errors->has('realisasi.*'))
                                                <div class="text-danger">
                                                    {{ $errors->first('realisasi.*') }}
                                                </div>
                                            @endif
                                        </div>
                                        <hr>
                                    @endif
                                @endforeach
                            @else
                                <input type="hidden" name="penilaianId" value="{{ $penilaian->id }}">
                                @foreach($penilaian->detailPenilaian as $detail)
                                    <div class="form-group">
                                        <label for="kinerja">
                                            {{ $detail->indikator->kinerja }}
                                        </label>

                                        <input type="hidden" name="indikatorId[]" value="{{ $detail->indikator->id }}">
                                        <input type="number" name="realisasi[]" class="form-control realisasi col-md-6" id="realisasi" value="{{ $detail->realisasi }}" max="{{ $detail->indikator->target }}" min="0" required>
                                        <p class="text-mini"><span class="text-danger">Bobot Nilai: {{ $indikator->bobot }} %</span>, <b>Target maksimal pencapaian : {{ $indikator->target }}</b><br>{{ $indikator->detailTarget }}</p>

                                        <!-- error -->
                                        @if($errors->has('realisasi.*'))
                                            <div class="text-danger">
                                                {{ $errors->first('realisasi.*') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="keterangan">
                                            Keterangan
                                        </label>

                                        <textarea name="keterangan[]" id="keterangan" rows="5" class="form-control">{{ $detail->keterangan }}</textarea>

                                        <!-- error -->
                                        @if($errors->has('realisasi.*'))
                                            <div class="text-danger">
                                                {{ $errors->first('realisasi.*') }}
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                            <hr>
                            <button type="submit" class="btn btn-primary btn-submit">Simpan</button>
                            @if(empty($id))
                                <button type="reset" class="btn btn-danger">Reset</button>
                            @else
                                <a href="{{ route('penilaian') }}" class="btn btn-danger">Batal</a>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('.realisasi').on('keyup', function(){
            var nilai = parseFloat($(this).val());
            var nilaiMax = parseFloat($(this).attr('max'));
            console.log(nilai);

            if(nilai > nilaiMax){
                $(this).addClass('is-invalid');
                $('.btn-submit').attr('disabled', 'disabled');
            }else{
                $(this).removeClass('is-invalid');
                $('.btn-submit').removeAttr('disabled');
            }
        });
    });
</script>
@endsection