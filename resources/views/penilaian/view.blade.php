<input type="hidden" class="getId" data-penilaian="{{ $penilaian->id }}">
<a href="{{ route('karyawan.print', ['id' => $penilaian->id]) }}" class="btn btn-primary mb-3"><i class="fas fa-print"></i> Print Data</a>
<div class="row">
    <div class="col-md-12">
        <label>Nama :</label><br>
        {{ $penilaian->karyawan->nama }}
    </div>
</div>
<hr style="margin-top:0">
<div class="row">
    <div class="col-md-12">
        <label>Jenis Kelamin :</label><br>
        {{ $penilaian->karyawan->jk }}
    </div>
</div>
<hr style="margin-top:0">
<div class="row">
    <div class="col-md-12">
        <label>Status :</label><br>
        {!! status($penilaian->karyawan->status) !!}
    </div>
</div>
<hr style="margin-top:0">
<div class="row">
    <div class="col-md-12">
        <label>Golongan / Dep :</label><br>
        {{ $penilaian->karyawan->golongan->nama.' / '.$penilaian->karyawan->golongan->divisi->namaDivisi }}
    </div>
</div>
<br>
<table class="table table-striped">
    <thead>
        <tr>
            <th>NO</th>
            <th width="35%">INDIKATOR</th>
            <th class="text-center">BOBOT</th>
            <th class="text-center">TARGET</th>
            <th class="text-center">REALISASI</th>
            <th class="text-center">NILAI(%)</th>
            <th>KETERANGAN</th>
        </tr>
    </thead>
    <tbody>
        <?php $grandTotal = 0; ?>
        @foreach($penilaian->detailPenilaian as $penilaian)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $penilaian->indikator->kinerja }}</td>
                <td class="text-right">{{ $penilaian->indikator->bobot }}</td>
                <td class="text-right">{{ $penilaian->indikator->target }}</td>
                <td class="text-right">{{ $penilaian->realisasi }}</td>
                <td class="text-right">{{ nilai($penilaian->realisasi, $penilaian->indikator-> target, $penilaian->indikator->bobot) }}</td>
                <td>{{ $penilaian->keterangan }}</td>
            </tr>

            <?php $grandTotal += nilai($penilaian->realisasi, $penilaian->indikator->target, $penilaian->indikator->bobot); ?>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="5" class="text-center">TOTAL</th>
            <th class="text-right">{{ $grandTotal }}</th>
            <td></td>
        </tr>
    </tfoot>
</table>