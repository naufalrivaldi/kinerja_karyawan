@extends('master')

@section('title', 'Karyawan')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Karyawan </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Data</li>
                        <li class="breadcrumb-item active" aria-current="page">Karyawan</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{ route('karyawan.form') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Tambah Karyawan</a>
                    </div>    
                    <div class="col-md-6">
                        <form action="{{ route('karyawan') }}" method="GET" class="formKaryawan">
                            <div class="row">
                                <div class="col">
                                    <select name="divisiId" class="form-control divisi" name="divisiId">
                                        <option value="">Pilih Divisi</option>
                                        @foreach($divisi as $divisi)
                                        <option value="{{ $divisi->id }}" {{ ($_GET)?($_GET['divisiId'] == $divisi->id)?'selected':'':'' }}>{{ $divisi->namaDivisi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <select name="status" class="form-control status" name="status">
                                        <option value="">Pilih Status</option>
                                        <option value="1" {{ ($_GET)?($_GET['status'] == 1)?'selected':'':'' }}>Aktif</option>
                                        <option value="2" {{ ($_GET)?($_GET['status'] == 2)?'selected':'':'' }}>Nonaktif</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped dataTable display">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>NIK</th>
                                <th>NAMA</th>
                                <th>JK</th>
                                <th>DIVISI / GOLONGAN</th>
                                <th>JABATAN</th>
                                <th>STATUS</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($karyawan as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $row->nik }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <td>{{ $row->jk }}</td>
                                    <td><span>{{ $row->golongan->divisi->namaDivisi }}<br><span class="badge badge-info">{{ $row->golongan->nama }}</span></td>
                                    <td><span class="badge badge-info">{{ $row->level->keterangan }}</span></td>
                                    <td>{!! status($row->status) !!}</td>
                                    <td>
                                        <a href="{{ route('karyawan.detail', ['karyawanId' => $row->id]) }}" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                                        @if($row->status == '1')
                                            <a href="{{ route('karyawan.nonaktif', ['id' => $row->id]) }}" class="btn btn-sm btn-danger"><i class="fas fa-times-circle"></i></a>
                                        @else
                                            <a href="{{ route('karyawan.aktif', ['id' => $row->id]) }}" class="btn btn-sm btn-success"><i class="fas fa-check-circle"></i></a>
                                        @endif
                                        
                                        <a href="{{ route('karyawan.edit', ['id' => $row->id]) }}" class="btn btn-sm btn-success"><i class="fas fa-cog"></i></a>
                                        <button class="btn btn-sm btn-danger btn-delete" data-id="{{ $row->id }}"><i class="far fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('.btn-delete').on('click', function(){
            var id = $(this).data('id');
            swal({
                title: "Hapus Data?",
                text: "Data yang dihapus tidak dapat dikembalikan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'POST',
                        data: {
                            '_token': '{{ csrf_token() }}',
                            'id': id
                        },
                        url: "{{ route('karyawan.destroy') }}",
                        success: function(data){
                            location.reload();
                        }
                    });
                }
            });
        });

        // form filter
        $('.divisi').on('change', function(){
            $('.formKaryawan').submit();
        });
        $('.status').on('change', function(){
            $('.formKaryawan').submit();
        });
    });
</script>
@endsection