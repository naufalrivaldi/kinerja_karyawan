<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link href="{{ asset('vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('libs/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/charts/chartist-bundle/chartist.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/charts/morris-bundle/morris.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/charts/c3charts/c3.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fonts/flag-icon-css/flag-icon.min.css') }}">
    <!-- datatable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

    <title>Penilaian Kinerja Karyawan</title>
    <style>
        .vCell {
            display: table-cell;
            vertical-align: middle !important;
        }

        .vCellTop {
            display: table-cell;
            vertical-align: top !important;
        }

        .cCell {
            display: table-cell;
            text-align: center !important;
        }

        .table{
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    <div class="container">
        <img src="images/header.jpg" alt="" width="100%">
        <hr>
        <!-- content -->
        <div class="row">
            <div class="col-md-12">
                <h3 class="display-5">Data Karyawan</h3>
                <table class="">
                    <tr>
                        <td width="15%">NIK</td>
                        <td width="1%">:</td>
                        <td><?= $penilaian->karyawan->nik ?></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><?= $penilaian->karyawan->nama ?></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td><?= $penilaian->karyawan->jk ?></td>
                    </tr>
                    <tr>
                        <td>Divisi / Golongan</td>
                        <td>:</td>
                        <td><?= $penilaian->karyawan->golongan->divisi->namaDivisi.' / '.$penilaian->karyawan->golongan->nama ?></td>
                    </tr>
                    <tr>
                        <td>Jabatan</td>
                        <td>:</td>
                        <td><?= $penilaian->karyawan->level->keterangan ?></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>:</td>
                        <td><?= ($penilaian->karyawan->status == 1)?'Aktif':'Nonaktif' ?></td>
                    </tr>
                </table>
                <hr>
                <h3 class="display-5">Periode</h3>
                <table class="">
                    <tr>
                        <td width="10%">Periode</td>
                        <td width="1%">:</td>
                        <td><?= $penilaian->periode->nama ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>
                            <?= setDate($penilaian->periode->tglAwal).' s/d '.setDate($penilaian->periode->tglAkhir) ?>
                        </td>
                    </tr>
                </table>
                <hr>
                
                <h3>Data Penilaian</h3>
                <!-- table penilaian -->
                <table class="table" border="1" width="100%">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th width="35%">INDIKATOR</th>
                            <th align="center">BOBOT</th>
                            <th align="center">TARGET</th>
                            <th align="center">REALISASI</th>
                            <th align="center">NILAI(%)</th>
                            <th>KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $grandTotal = 0; ?>
                        <?php foreach($penilaian->detailPenilaian as $penilaian): ?>
                            <tr>
                                <td valign="top"><?= $no++ ?></td>
                                <td valign="top"><?= $penilaian->indikator->kinerja ?></td>
                                <td align="right" valign="top"><?= $penilaian->indikator->bobot ?></td>
                                <td align="right" valign="top"><?= $penilaian->indikator->target ?></td>
                                <td align="right" valign="top"><?= $penilaian->realisasi ?></td>
                                <td align="right" valign="top"><?= nilai($penilaian->realisasi, $penilaian->indikator-> target, $penilaian->indikator->bobot) ?></td>
                                <td valign="top"><?= $penilaian->keterangan ?></td>
                            </tr>

                            <?php $grandTotal += nilai($penilaian->realisasi, $penilaian->indikator->target, $penilaian->indikator->bobot); ?>
                        <?php endforeach ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="5" align="center">TOTAL</th>
                            <th align="right"><?= $grandTotal ?></th>
                            <td></td>
                        </tr>
                    </tfoot> 
                </table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="{{ asset('vendor/jquery/jquery-3.3.1.min.js') }}"></script>
    <!-- bootstap bundle js -->
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
    <!-- slimscroll js -->
    <script src="{{ asset('vendor/slimscroll/jquery.slimscroll.js') }}"></script>
    <!-- main js -->
    <script src="{{ asset('libs/js/main-js.js') }}"></script>
    <!-- chart chartist js -->
    <script src="{{ asset('vendor/charts/chartist-bundle/chartist.min.js') }}"></script>
    <!-- sparkline js -->
    <script src="{{ asset('vendor/charts/sparkline/jquery.sparkline.js') }}"></script>
    <!-- chart c3 js -->
    <script src="{{ asset('vendor/charts/c3charts/c3.min.js') }}"></script>
    <script src="{{ asset('vendor/charts/c3charts/d3-5.4.0.min.js') }}"></script>
    <script src="{{ asset('vendor/charts/c3charts/C3chartjs.js') }}"></script>
    <!-- datatable -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        $(document).ready( function () {
            $('.dataTable').DataTable();
        } );
        
        // tooltip
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</body>
 
</html>