@extends('master')

@section('title', 'Karyawan')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Karyawan </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Data</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ route('karyawan') }}" class="breadcrumb-link">Karyawan</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Form</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="display-7">{{ (!empty($id))? 'Edit' : 'Tambah' }} Karyawan</h3>
            </div>
            <div class="card-body">
                <form action="{{ (empty($id))? route('karyawan.store') : route('karyawan.update')  }}" method="POST">
                    @csrf
                    @if(!empty($id))
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $id }}">
                        <input type="hidden" class="golonganId" value="{{ $karyawan->golonganId }}">
                    @endif
                    <div class="row justify-content-md-center">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nik">NIK</label>
                                <input type="text" name="nik" class="form-control" id="nik" placeholder="xxxx.xxxx" value="{{ (!empty($id))? $karyawan->nik : '' }}" onkeyup="convertToMin(this);" onkeypress="return hanyaAngka(event)" maxlength="9">

                                <!-- error -->
                                @if($errors->has('nik'))
                                    <div class="text-danger">
                                        {{ $errors->first('nik') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama</label>
                                <input type="text" name="nama" class="form-control" id="nama" value="{{ (!empty($id))? $karyawan->nama : '' }}">

                                <!-- error -->
                                @if($errors->has('nama'))
                                    <div class="text-danger">
                                        {{ $errors->first('nama') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="jk">Jenis Kelamin</label>
                                <select name="jk" id="jk" class="form-control jk">
                                    <option value="">Pilih</option>
                                    <option value="L" {{ (!empty($id))? ($karyawan->jk == 'L')? 'selected' : '' : '' }}>Laki - Laki</option>
                                    <option value="P" {{ (!empty($id))? ($karyawan->jk == 'P')? 'selected' : '' : '' }}>Perempuan</option>
                                </select>

                                <!-- error -->
                                @if($errors->has('jk'))
                                    <div class="text-danger">
                                        {{ $errors->first('jk') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="level">Jabatan</label>
                                <select name="levelId" id="level" class="form-control">
                                    <option value="">Pilih</option>
                                    @foreach($level as $level)
                                        <option value="{{ $level->id }}" {{ (!empty($id))? ($karyawan->levelId == $level->id)? 'selected' : '' : '' }}>{{ $level->keterangan }}</option>
                                    @endforeach
                                </select>

                                <!-- error -->
                                @if($errors->has('levelId'))
                                    <div class="text-danger">
                                        {{ $errors->first('levelId') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="divisi">Divisi</label>
                                <select name="divisiId" id="divisi" class="form-control">
                                    <option value="">Pilih</option>
                                    @foreach($divisi as $divisi)
                                        <option value="{{ $divisi->id }}" {{ (!empty($id))? ($karyawan->golongan->divisi->id == $divisi->id)? 'selected' : '' : '' }}>{{ $divisi->inisial.' - '.$divisi->namaDivisi }}</option>
                                    @endforeach
                                </select>

                                <!-- error -->
                                @if($errors->has('divisiId'))
                                    <div class="text-danger">
                                        {{ $errors->first('divisiId') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="golongan">Golongan</label>
                                <select name="golonganId" id="golongan" class="form-control fillGolongan">
                                </select>
                                <span class="text-small text-danger">*Pilih divisi terlebih dahulu.</span>
                                <!-- error -->
                                @if($errors->has('golonganId'))
                                    <div class="text-danger">
                                        {{ $errors->first('golonganId') }}
                                    </div>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        @if(empty($id))
        $('#golongan').attr('disabled', 'disabled');
        @endif
        
        // klik divisi
        $('#divisi').on('change', function(){
            var divisiId = $(this).val();
            $.ajax({
                type: 'GET',
                data: "id="+divisiId,
                url: "{{ route('karyawan.fillGolongan') }}",
                success: function(data){
                    $('#golongan').removeAttr('disabled');
                    $('.text-small').empty();
                    $('.fillGolongan').empty();
                    $('.fillGolongan').append(data);
                }
            });
        });

        var divisiId = '';
        var golonganId = '';
        // edit selected
        @if(!empty($id))
            $('#divisi option').each(function() {
                if($(this).is(':selected')){
                    divisiId = $(this).val();
                }
            });
            golonganId = $('.golonganId').val();
            console.log(golonganId);

            $.ajax({
                type: 'GET',
                data: {
                    'divisiId' : divisiId,
                    'golonganId' : golonganId,
                },
                url: "{{ route('karyawan.fillGolonganEdit') }}",
                success: function(data){
                    $('.text-small').empty();
                    $('.fillGolongan').empty();
                    $('.fillGolongan').append(data);
                }
            });
        @endif
    });
</script>
@endsection