@extends('master')

@section('title', 'Karyawan')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Detail Karyawan </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Data</li>
                        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('karyawan') }}">Karyawan</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Detail</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Data Karyawan</h3>
            </div>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <td width="20%">NIK</td>
                        <td width="1%">:</td>
                        <td>{{ $karyawan->nik }}</td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{ $karyawan->nama }}</td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>{{ $karyawan->jk }}</td>
                    </tr>
                    <tr>
                        <td>Divisi / Golongan</td>
                        <td>:</td>
                        <td>{{ $karyawan->golongan->divisi->namaDivisi.' / '.$karyawan->golongan->nama }}</td>
                    </tr>
                    <tr>
                        <td>Jabatan</td>
                        <td>:</td>
                        <td>{{ $karyawan->level->keterangan }}</td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>:</td>
                        <td>{!! status($karyawan->status) !!}</td>
                    </tr>
                    <tr>
                        <td>Rata - rata kinerja</td>
                        <td>:</td>
                        <td>{{ (!empty($penilaian))?rataKinerja($penilaian):'0' }} %</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Data Penilaian</h3>
            </div>
            <div class="card-body">
                <canvas id="chartUser"></canvas>
                
                <!-- table -->
                <table class="table table-striped dataTable display">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>PERIODE</th>
                            <th>HASIL PENILAIAN (%)</th>
                            <th>AKSI</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($penilaian as $row)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $row->periode->nama }}</td>
                            <td>{{ sum($row->detailPenilaian) }}</td>
                            <td>
                                <!-- view -->
                                <button class="btn btn-info btn-sm btn-modal" data-toggle="modal" data-target="#modalPenilaian" data-penilaianid="{{ $row->id }}"><i class="fas fa-eye"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="modalPenilaian" tabindex="-1" role="dialog" aria-labelledby="modalPenilaian" aria-hidden="true">
    <div class="modal-dialog modalWidthLg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title display-7" id="modalPenilaianTitle">Detail Penilaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="view"></div>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).on('click', '.btn-modal', function(){
        var penilaianId = $(this).data('penilaianid');

        $('.view').empty();
        $.ajax({
            type: "GET",
            url: "{{ route('karyawan.view.data') }}",
            data: {
                'penilaianId' : penilaianId
            },
            success: function(data){
                $('.view').append(data);
            }
        });
    });

    $(document).ready(function(){
        $('.btn-delete').on('click', function(){
            var id = $(this).data('id');
            swal({
                title: "Hapus Data?",
                text: "Data yang dihapus tidak dapat dikembalikan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'POST',
                        data: {
                            '_token': '{{ csrf_token() }}',
                            'id': id
                        },
                        url: "{{ route('karyawan.destroy') }}",
                        success: function(data){
                            location.reload();
                        }
                    });
                }
            });
        });

        // chart
        var chartUser = $('#chartUser');
        var label = @json($arrayPeriode);
        var nilai = @json($arrayNilai);
        var chart = new Chart(chartUser, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: label,
                datasets: [{
                    label: 'Kinerja Per Periode',
                    borderColor: 'rgb(0, 0, 180)',
                    data: nilai
                }]
            },

            // Configuration options go here
            options: {}
        });
    });
</script>
@endsection