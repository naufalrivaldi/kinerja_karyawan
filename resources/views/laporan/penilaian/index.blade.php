@extends('master')

@section('title', 'Laporan Penilaian')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Laporan Penilaian Karyawan</h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Laporan</li>
                        <li class="breadcrumb-item active" aria-current="page">Penilaian Karyawan</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            @if(!empty($setPeriode))
            <div class="card-header">
                <h3 class="display-5">Periode</h3>
                <table class="table">
                    <tr>
                        <td width="10%">Periode</td>
                        <td width="1%">:</td>
                        <td>{{ $setPeriode->nama }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>
                            <span class="badge badge-info">{{ setDate($setPeriode->tglAwal).' s/d '.setDate($setPeriode->tglAkhir) }}</span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="card-header">
                <form action="" method="GET">
                    <div class="row">
                        <div class="col">
                            <label>Periode</label>
                            <select name="periodeId" class="form-control">
                                @foreach($periode as $periode)
                                    <option value="{{ $periode->id }}" {{ ($setPeriode->id == $periode->id) ? 'selected' : '' }}>{{ $periode->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <label>Divisi</label>
                            <select name="divisiId" id="divisi" class="form-control">
                                <option value="">Pilih</option>
                                @foreach($divisi as $divisi)
                                    <option value="{{ $divisi->id }}" {{ ($_GET) ? ($divisi->id == $_GET['divisiId'])? 'selected' : '' : '' }}>{{ $divisi->namaDivisi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <label>Golongan <span class="text-small text-danger">*Pilih divisi.</span></label>
                            <select name="golonganId" class="form-control fillGolongan">
                                <option value="">Pilih</option>
                                @if($_GET)
                                    <option value="0" {{ ($_GET['golonganId'] == '0')? 'selected' : '' }}>Semua Golongan</option>
                                    @foreach($golonganEdit as $goled)
                                        <option value="{{ $goled->id }}" {{ ($goled->id == $_GET['golonganId'])? 'selected' : '' }}>{{ $goled->nama }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col">
                            <label class="text-white">asd</label><br>
                            <input type="submit" class="btn btn-success" value="Cari">
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-body">
                <!-- grafik -->

                <!-- grafik -->
                <h3>Data Penilaian</h3>
                <hr>
                <!-- Tab Pane Golongan -->
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        @foreach($setGolongan as $gol1)
                            <a class="nav-item nav-link {{ ($no == 1) ? 'active' : '' }}" id="nav-home-tab" data-toggle="tab" href="#nav{{ $no }}" role="tab" aria-selected="{{ ($no == 1) ? 'true' : '' }}" aria-controls="nav{{ $no++ }}">{{ $gol1->nama }}</a>
                        @endforeach
                    </div>
                </nav>
                
                <div class="tab-content" id="nav-tabContent">
                    <?php $noNav = 1 ?>
                    @foreach($setGolongan as $gol2)
                        <?php 
                            $noTable = 1; 
                            $dataIndikator = HelpPenilaian::setGolongan($gol2->id, $setPeriode->id);
                            $bonusGaji = HelpPenilaian::setGaji($gol2->id, $setPeriode->id);
                        ?>
                        <div class="tab-pane fade show {{ ($noNav == 1) ? 'active' : '' }}" id="nav{{ $noNav }}" role="tabpanel" aria-labelledby="nav{{ $noNav++ }}-tab">
                            <br>

                            <!-- table penilaian -->
                            <div class="table-responsive">
                                <?php
                                    $url='periodeId='.$setPeriode->id.'&divisiId=&golonganId=';
                                    if($_GET){
                                        $url = $_SERVER['QUERY_STRING'];
                                    }
                                ?>
                                <a href="{{ route('laporan.penilaian.print') }}?{{ $url }}&golonganId2={{ $gol2->id }}" class="btn btn-primary mb-3"><i class="fas fa-print"></i> Print Data</a>
                                <table class="table table-bordered dataTable mt-2" width="100%">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" class="vCell">NO</th>
                                            <th rowspan="2" class="vCell">NAMA KARYAWAN</th>
                                            <th colspan="{{ $dataIndikator->count() }}" class="cCell">INDIKATOR</th>
                                            <th rowspan="2" class="vCell text-center">TOTAL<br>(%)</th>

                                            @if(Auth::guard('penilai')->user()->levelId == '5')
                                                <th rowspan="2" class="vCell text-center">BONUS GAJI<br>(Rp)</th>
                                            @endif

                                            <th rowspan="2" class="vCell text-center">AKSI</th>
                                        </tr>
                                        <tr>
                                            <?php $idx = 1; ?>
                                            @foreach($dataIndikator as $ind)
                                                <th class="cCell">Ind{{ $idx++ }}<br>{{ '('.$ind->indikator->bobot.'%)' }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($penilaian as $pnl)
                                            <?php
                                                $total = 0;
                                            ?>
                                            @if($gol2->id == $pnl->karyawan->golonganId)
                                                <tr>
                                                    <td class="vCellTop">{{ $noTable++ }}</td>
                                                    <td class="vCellTop">{{ $pnl->karyawan->nama }}</td>
                                                    @foreach($pnl->detailPenilaian as $detail)
                                                        <?php
                                                            $total += nilai($detail->realisasi, $detail->indikator->target, $detail->indikator->bobot);
                                                        ?>
                                                        <td class="vCellTop" align="right" data-toggle="tooltip" data-placement="bottom" title="{{ $detail->keterangan }}">
                                                            {{ nilai($detail->realisasi, $detail->indikator->target, $detail->indikator->bobot) }}
                                                        </td> 
                                                    @endforeach
                                                    <td class="vCellTop" align="right">{{ $total }}</td>

                                                    @if(Auth::guard('penilai')->user()->levelId == '5')
                                                        <td class="vCellTop" align="right">{{ number_format($pnl->karyawan->golongan->bonusGaji * $total / 100) }}</td>
                                                    @endif
                                                    <td>
                                                        <button class="btn btn-info btn-sm btn-modal" data-toggle="modal" data-target="#modalPenilaian" data-karyawanid="{{ $pnl->karyawanId }}" data-periodeid="{{ $setPeriode->id }}"><i class="fas fa-eye"></i></button>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- table penilaian -->
                            <hr>
                            <h5>Indikator :</h5>
                            <?php $no=1; ?>
                            @foreach($dataIndikator as $ind)
                                <table width="100%">
                                    <tr>
                                        <td width="10%">Ind{{ $no++ }}</td>
                                        <td width="2%">:</td>
                                        <td>{{ $ind->indikator->kinerja.' ('.$ind->indikator->detailTarget.')' }}</td>
                                    </tr>
                                </table>
                            @endforeach
                        </div>
                    @endforeach
                </div>
                <!-- Tab Pane Golongan -->

            </div>
            @else
            <div class="card-body">
                <div class="alert alert-danger" role="alert">
                    Periode belum ditambahkan!
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="modalPenilaian" tabindex="-1" role="dialog" aria-labelledby="modalPenilaian" aria-hidden="true">
    <div class="modal-dialog modalWidthLg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title display-7" id="modalPenilaianTitle">Detail Penilaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="view"></div>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).on('click', '.btn-modal', function(){
        var karyawanId = $(this).data('karyawanid');
        var periodeId = $(this).data('periodeid');

        $('.view').empty();
        $.ajax({
            type: "GET",
            url: "{{ route('penilaian.view') }}",
            data: {
                'karyawanId' : karyawanId,
                'periodeId' : periodeId
            },
            success: function(data){
                $('.view').append(data);
            }
        });
    });
    
    $(document).ready(function(){
        // klik divisi
        $('#divisi').on('change', function(){
            var divisiId = $(this).val();
            $.ajax({
                type: 'GET',
                data: "id="+divisiId,
                url: "{{ route('laporan.penilaian.fillGolongan') }}",
                success: function(data){
                    $('.text-small').empty();
                    $('.fillGolongan').empty();
                    $('.fillGolongan').append(data);
                }
            });
        });
    });
</script>
@endsection