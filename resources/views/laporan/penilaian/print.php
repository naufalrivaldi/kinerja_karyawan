<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link href="{{ asset('vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('libs/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/charts/chartist-bundle/chartist.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/charts/morris-bundle/morris.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/charts/c3charts/c3.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fonts/flag-icon-css/flag-icon.min.css') }}">
    <!-- datatable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

    <title>Penilaian Kinerja Karyawan</title>
    <style>
        .vCell {
            display: table-cell;
            vertical-align: middle !important;
        }

        .vCellTop {
            display: table-cell;
            vertical-align: top !important;
        }

        .cCell {
            display: table-cell;
            text-align: center !important;
        }

        .table{
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    <div class="container">
        <img src="images/header.jpg" alt="" width="100%">
        <hr>
        <!-- content -->
        <div class="row">
            <div class="col-md-12">
                <h3 class="display-5">Periode</h3>
                <table class="">
                    <tr>
                        <td width="10%">Periode</td>
                        <td width="1%">:</td>
                        <td><?= $setPeriode->nama ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>
                            <?= setDate($setPeriode->tglAwal).' s/d '.setDate($setPeriode->tglAkhir) ?>
                        </td>
                    </tr>
                </table>
                <hr>
                
                <h3>Data Penilaian</h3>
                <!-- Tab Pane Golongan -->
                <?php foreach($setGolongan as $gol1): ?>
                    <p>Golongan : <?= $gol1->nama ?></p>
                <?php endforeach ?>
                
                <?php $no = 1 ?>
                <?php foreach($setGolongan as $gol2): ?>
                <?php 
                    $noTable = 1; 
                    $dataIndikator = HelpPenilaian::setGolongan($gol2->id, $setPeriode->id);
                    $bonusGaji = HelpPenilaian::setGaji($gol2->id, $setPeriode->id);
                ?>
                    <!-- table penilaian -->
                    <table class="table" border="1" width="100%">
                        <thead>
                            <tr>
                                <th class="vCell cCell" rowspan="2">NO</th>
                                <th class="vCell cCell" rowspan="2">NAMA KARYAWAN</th>
                                <th colspan="<?= $dataIndikator->count() ?>" class="cCell">INDIKATOR</th>
                                <th class="vCell cCell text-center" rowspan="2">TOTAL<br>(%)</th>

                                <?php if(Auth::guard('penilai')->user()->levelId == '5'): ?>
                                    <th class="vCell cCell text-center" rowspan="2">BONUS GAJI<br>(Rp)</th>
                                <?php endif ?>
                            </tr>
                            <tr>
                                <?php $idx = 1; ?>
                                <?php foreach($dataIndikator as $ind): ?>
                                    <th class="cCell">Ind<?= $idx++ ?><br><?= '('.$ind->indikator->bobot.'%)' ?></th>
                                <?php endforeach ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($penilaian as $pnl): ?>
                                <?php
                                    $total = 0;
                                ?>
                                <?php if($gol2->id == $pnl->karyawan->golonganId): ?>
                                    <tr>
                                        <td class="vCellTop"><?= $noTable++ ?></td>
                                        <td class="vCellTop"><?= $pnl->karyawan->nama ?></td>
                                        <?php foreach($pnl->detailPenilaian as $detail): ?>
                                            <?php
                                                $total += nilai($detail->realisasi, $detail->indikator->target, $detail->indikator->bobot);
                                            ?>
                                            <td class="vCellTop" align="right">
                                                <?= nilai($detail->realisasi, $detail->indikator->target, $detail->indikator->bobot) ?>
                                            </td> 
                                        <?php endforeach ?>
                                        <td class="vCellTop" align="right"><?= $total ?></td>

                                        <?php if(Auth::guard('penilai')->user()->levelId == '5'): ?>
                                            <td class="vCellTop" align="right"><?= number_format($pnl->karyawan->golongan->bonusGaji * $total / 100) ?></td>
                                        <?php endif ?>
                                    </tr>
                                <?php endif ?>
                            <?php endforeach ?>
                        </tbody> 
                    </table>
                    
                    <h3>Indikator :</h3>
                    <?php $no=1; ?>
                    <?php foreach($dataIndikator as $ind): ?>
                        <table width="100%">
                            <tr>
                                <td width="10%">Ind<?= $no++ ?></td>
                                <td width="2%">:</td>
                                <td><?= $ind->indikator->kinerja.' ('.$ind->indikator->detailTarget.')' ?></td>
                            </tr>
                        </table>
                    <?php endforeach ?>
                <?php endforeach ?>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="{{ asset('vendor/jquery/jquery-3.3.1.min.js') }}"></script>
    <!-- bootstap bundle js -->
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
    <!-- slimscroll js -->
    <script src="{{ asset('vendor/slimscroll/jquery.slimscroll.js') }}"></script>
    <!-- main js -->
    <script src="{{ asset('libs/js/main-js.js') }}"></script>
    <!-- chart chartist js -->
    <script src="{{ asset('vendor/charts/chartist-bundle/chartist.min.js') }}"></script>
    <!-- sparkline js -->
    <script src="{{ asset('vendor/charts/sparkline/jquery.sparkline.js') }}"></script>
    <!-- chart c3 js -->
    <script src="{{ asset('vendor/charts/c3charts/c3.min.js') }}"></script>
    <script src="{{ asset('vendor/charts/c3charts/d3-5.4.0.min.js') }}"></script>
    <script src="{{ asset('vendor/charts/c3charts/C3chartjs.js') }}"></script>
    <!-- datatable -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        $(document).ready( function () {
            $('.dataTable').DataTable();
        } );
        
        // tooltip
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</body>
 
</html>