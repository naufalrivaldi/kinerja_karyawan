@extends('master')

@section('title', 'Divisi')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Divisi </h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">Data</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ route('divisi') }}" class="breadcrumb-link">Divisi</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Form</li>
                    </ol>
                </nav>
            </div>

            <!-- contohnya ni -->
            <!-- <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">E-Commerce Dashboard Template</li>
                    </ol>
                </nav>
            </div> -->
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="display-7">{{ (!empty($id))? 'Edit' : 'Tambah' }} Divisi</h3>
            </div>
            <div class="card-body">
                <form action="{{ (empty($id))? route('divisi.store') : route('divisi.update')  }}" method="POST">
                    @csrf
                    @if(!empty($id))
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <div class="row justify-content-md-center">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inisial">Inisial</label>
                                <input type="text" name="inisial" class="form-control" id="inisial" aria-describedby="inisialHelp" value="{{ (!empty($id))? $divisi->inisial : '' }}">
                                <small id="inisialHelp" class="form-text text-muted">Inisial departemen terkait.</small>

                                <!-- error -->
                                @if($errors->has('inisial'))
                                    <div class="text-danger">
                                        {{ $errors->first('inisial') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="namaDivisi">Nama Divisi</label>
                                <input type="text" name="namaDivisi" class="form-control" id="namaDivisi" value="{{ (!empty($id))? $divisi->namaDivisi : '' }}">

                                <!-- error -->
                                @if($errors->has('namaDivisi'))
                                    <div class="text-danger">
                                        {{ $errors->first('namaDivisi') }}
                                    </div>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection