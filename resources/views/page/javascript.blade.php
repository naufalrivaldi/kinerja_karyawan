<!-- jquery 3.3.1 -->
<script src="{{ asset('vendor/jquery/jquery-3.3.1.min.js') }}"></script>
<!-- bootstap bundle js -->
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
<!-- slimscroll js -->
<script src="{{ asset('vendor/slimscroll/jquery.slimscroll.js') }}"></script>
<!-- main js -->
<script src="{{ asset('libs/js/main-js.js') }}"></script>
<!-- chart chartist js -->
<script src="{{ asset('vendor/charts/chartist-bundle/chartist.min.js') }}"></script>
<!-- sparkline js -->
<script src="{{ asset('vendor/charts/sparkline/jquery.sparkline.js') }}"></script>
<!-- chart c3 js -->
<script src="{{ asset('vendor/charts/c3charts/c3.min.js') }}"></script>
<script src="{{ asset('vendor/charts/c3charts/d3-5.4.0.min.js') }}"></script>
<script src="{{ asset('vendor/charts/c3charts/C3chartjs.js') }}"></script>
<!-- datatable -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>
<!-- sweetalert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- chartjs -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.js"></script>

<script>
    $(document).ready( function () {
        $('.dataTable').DataTable({
            fixedHeader:{
                header:true,
                headerOffset: $('.navbar').outerHeight()
            }
        });
        $('.dataTableLaporan').DataTable({
            scrollX:        true,
            scrollCollapse: false,
            fixedColumns:   {
                leftColumns: 2,
            }
        });
    } );
    
    
    // tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    function convertToMin(objek) {
        separator = ".";
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 4) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + separator + c;
            } else {
                c = b.substr(i - 1, 1) + c;
            }
        }
        objek.value = c;
    }

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        
        return true;
    }
</script>

@yield('script')
