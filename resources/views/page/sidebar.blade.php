<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        Menu
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('dashboard') }}"><i class="fas fa-fw fa-tachometer-alt"></i>Dashboard </a>
                    </li>
                    
                    @if(Auth::guard('penilai')->user()->levelId == '5')
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fas fa-fw fa-chart-pie"></i>Data Master</a>
                        <div id="submenu-3" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('penilai') }}">Penilai</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('karyawan') }}">Karyawan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('divisi') }}">Divisi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('golongan') }}">Golongan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('periode') }}">Periode</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('indikator') }}">Indikator</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endif

                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-4" aria-controls="submenu-4"><i class="far fa-fw fa-edit"></i>Penilaian</a>
                        <div id="submenu-4" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('penilaian') }}">Penilaian Karyawan</a>
                                </li>
                                
                                @if(Auth::guard('penilai')->user()->levelId > '2' && Auth::guard('penilai')->user()->levelId != '5')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('verifikasi') }}">Verifikasi Penilaian</a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-5"><i class="fas fa-fw fa-file"></i>Laporan</a>
                        <div id="submenu-5" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('laporan.penilaian') }}">Penilaian Karyawan</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>