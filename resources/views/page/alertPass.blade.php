@if (HelpPenilaian::defaultPass())
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        Password anda masih menggunakan password default. silahkan ubah password <a href="{{ route('login.ubahpass') }}">disini</a>.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif