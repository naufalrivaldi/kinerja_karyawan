<div class="dashboard-header">
    <nav class="navbar navbar-expand-lg bg-white fixed-top">
        <a class="navbar-brand" href="#">
            <img src="{{ asset('images/logo-cwa.png') }}" alt="logo-cwa" width="50">
            PENILAIAN KINERJA KARYAWAN
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto navbar-right-top">
                <li class="nav-item dropdown nav-user">
                    <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                        <div class="nav-user-info">
                            <h5 class="mb-0 text-white nav-user-name">
                                {{ Auth::guard('penilai')->user()->nama }}
                            </h5>
                            <span class="status"></span><span class="ml-2">
                                {{ Auth::guard('penilai')->user()->divisi->namaDivisi }} / <span class="badge badge-light">{{ Auth::guard('penilai')->user()->level->keterangan }}</span>
                            </span>
                        </div>
                        <a class="dropdown-item" href="{{ route('login.ubahpass') }}"><i class="fas fa-key mr-2"></i>Ubah Password</a>
                        <a class="dropdown-item" href="{{ route('login.logout') }}"><i class="fas fa-power-off mr-2"></i>Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>