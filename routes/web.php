<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login
Route::get('/', 'AuthController@index')->name('login');
Route::post('/signin', 'AuthController@signin')->name('login.signin');


Route::group(['middleware' => ['auth:penilai']], function(){
    Route::get('/logout', 'AuthController@logout')->name('login.logout');
    Route::get('/ubahpass', 'AuthController@ubahpass')->name('login.ubahpass');
    Route::post('/ubahpass/store', 'AuthController@passStore')->name('login.ubahpass.store');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    // penilai
    Route::group(['prefix' => 'penilai', 'middleware' => ['checkLevel:5']], function(){
        Route::get('/', 'PenilaiController@index')->name('penilai');
        Route::get('/form', 'PenilaiController@form')->name('penilai.form');
        Route::get('/{id}/edit', 'PenilaiController@edit')->name('penilai.edit');
        Route::get('/{id}/reset', 'PenilaiController@reset')->name('penilai.reset');
        Route::post('/store', 'PenilaiController@store')->name('penilai.store');
        Route::put('/update', 'PenilaiController@update')->name('penilai.update');
        Route::post('/destroy', 'PenilaiController@destroy')->name('penilai.destroy');
    });

    // karyawan
    Route::group(['prefix' => 'karyawan', 'middleware' => ['checkLevel:5']], function(){
        Route::get('/', 'KaryawanController@index')->name('karyawan');
        Route::get('/form', 'KaryawanController@form')->name('karyawan.form');
        Route::get('/{id}/edit', 'KaryawanController@edit')->name('karyawan.edit');
        Route::get('/{id}/detail', 'KaryawanController@detail')->name('karyawan.detail');
    
        Route::get('/view', 'KaryawanController@view')->name('karyawan.view.data');
        Route::post('/store', 'KaryawanController@store')->name('karyawan.store');
        Route::put('/update', 'KaryawanController@update')->name('karyawan.update');
        Route::post('/destroy', 'KaryawanController@destroy')->name('karyawan.destroy');

        Route::get('/fillgolongan', 'KaryawanController@golongan')->name('karyawan.fillGolongan');
        Route::get('/fillgolonganedit', 'KaryawanController@golonganEdit')->name('karyawan.fillGolonganEdit');
        Route::get('/{id}/aktif', 'KaryawanController@aktif')->name('karyawan.aktif');
        Route::get('/{id}/nonaktif', 'KaryawanController@nonaktif')->name('karyawan.nonaktif');
    });

    Route::group(['prefix' => 'karyawan'], function(){
        Route::get('/{id}/print', 'KaryawanController@print')->name('karyawan.print');
    });

    // divisi
    Route::group(['prefix' => 'divisi', 'middleware' => ['checkLevel:5']], function(){
        Route::get('/', 'DivisiController@index')->name('divisi');
        Route::get('/form', 'DivisiController@form')->name('divisi.form');
        Route::get('/{id}/edit', 'DivisiController@edit')->name('divisi.edit');
        Route::post('/store', 'DivisiController@store')->name('divisi.store');
        Route::put('/update', 'DivisiController@update')->name('divisi.update');
        Route::post('/destroy', 'DivisiController@destroy')->name('divisi.destroy');
    });

    // Golongan
    Route::group(['prefix' => 'golongan', 'middleware' => ['checkLevel:5']], function(){
        Route::get('/', 'GolonganController@index')->name('golongan');
        Route::get('/form', 'GolonganController@form')->name('golongan.form');
        Route::get('/{id}/detail', 'GolonganController@detail')->name('golongan.detail');
        Route::get('/{id}/edit', 'GolonganController@edit')->name('golongan.edit');
        Route::post('/store', 'GolonganController@store')->name('golongan.store');
        Route::put('/update', 'GolonganController@update')->name('golongan.update');
        Route::post('/destroy', 'GolonganController@destroy')->name('golongan.destroy');
    });

    // periode
    Route::group(['prefix' => 'periode', 'middleware' => ['checkLevel:5']], function(){
        Route::get('/', 'PeriodeController@index')->name('periode');
        Route::get('/form', 'PeriodeController@form')->name('periode.form');
        Route::get('/{id}/edit', 'PeriodeController@edit')->name('periode.edit');
        Route::post('/store', 'PeriodeController@store')->name('periode.store');
        Route::put('/update', 'PeriodeController@update')->name('periode.update');
        Route::post('/destroy', 'PeriodeController@destroy')->name('periode.destroy');

        Route::get('/{id}/aktif', 'PeriodeController@aktif')->name('periode.aktif');
        Route::get('/{id}/nonaktif', 'PeriodeController@nonaktif')->name('periode.nonaktif');
    });

    // Indikator
    Route::group(['prefix' => 'indikator', 'middleware' => ['checkLevel:5']], function(){
        Route::get('/', 'IndikatorController@index')->name('indikator');
        Route::get('/{id}/form', 'IndikatorController@form')->name('indikator.form');
        Route::get('/{id}/detail', 'IndikatorController@detail')->name('indikator.detail');
        Route::get('/{id}/edit', 'IndikatorController@edit')->name('indikator.edit');
        Route::get('/nonaktif', 'IndikatorController@nonaktif')->name('indikator.nonaktif');
        Route::post('/store', 'IndikatorController@store')->name('indikator.store');
        Route::put('/update', 'IndikatorController@update')->name('indikator.update');
        Route::post('/destroy', 'IndikatorController@destroy')->name('indikator.destroy');

        Route::get('/view', 'IndikatorController@view')->name('indikator.view');
    });

    // penilaian
    Route::group(['prefix' => 'penilaian', 'middleware' => ['checkLevel:2,3,4,5']], function(){
        Route::get('/', 'PenilaianController@index')->name('penilaian');
        Route::get('/{karyawanId}/form', 'PenilaianController@form')->name('penilaian.form');
        Route::get('/{karyawanId}/edit', 'PenilaianController@edit')->name('penilaian.edit');
        Route::get('/{periodeId}/{golonganId}/kirim', 'PenilaianController@kirim')->name('penilaian.kirim');
        Route::get('/view', 'PenilaianController@view')->name('penilaian.view');
        Route::post('/store', 'PenilaianController@store')->name('penilaian.store');
        Route::put('/update', 'PenilaianController@update')->name('penilaian.update');
    });

    // verifikasi
    Route::group(['prefix' => 'verifikasi', 'middleware' => ['checkLevel:3,4']], function(){
        Route::get('/', 'VerifikasiController@index')->name('verifikasi');
        Route::get('/{id}/{periodeId}/view', 'VerifikasiController@view')->name('verifikasi.view');
        Route::get('penilaian', 'VerifikasiController@viewPenilaian')->name('verifikasi.view.penilaian');
        Route::post('acc', 'VerifikasiController@acc')->name('verifikasi.acc');
        Route::post('accArray', 'VerifikasiController@accArray')->name('verifikasi.acc.array');
    });

    // Laporan
    Route::group(['prefix' => 'laporan', 'middleware' => ['checkLevel:2,3,4,5']], function(){
        // Laporan penilaian
        Route::group(['prefix' => 'penilaian'], function(){
            Route::get('/', 'LaporanPenilaianController@index')->name('laporan.penilaian');
            Route::get('/fillGolongan', 'LaporanPenilaianController@golongan')->name('laporan.penilaian.fillGolongan');
            Route::get('/print', 'LaporanPenilaianController@print')->name('laporan.penilaian.print');
        });
    });
});