<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJabatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('golongan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama', 20);
            $table->double('bonusGaji');
            $table->unsignedBigInteger('divisiId');

            // fk
            $table->foreign('divisiId')
                    ->references('id')
                    ->on('divisi')
                    ->onUpdated('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('golongan');
    }
}
