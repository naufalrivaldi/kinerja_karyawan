<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama', 50);
            $table->string('nik', 9);
            $table->string('password', 100);
            $table->string('remember_token');
            $table->unsignedBigInteger('levelId');
            $table->unsignedBigInteger('divisiId');

            // fk
            $table->foreign('levelId')
                    ->references('id')
                    ->on('level')
                    ->onDelete('cascade');

            $table->foreign('divisiId')
                    ->references('id')
                    ->on('divisi')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilai');
    }
}
