<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nik', 9);
            $table->string('nama', 50);
            $table->enum('jk', ['L', 'P']);
            $table->enum('status', ['1', '2']);
            $table->unsignedBigInteger('golonganId');
            $table->unsignedBigInteger('levelId');

            // fk
            $table->foreign('golonganId')
                    ->references('id')
                    ->on('golongan')
                    ->onDelete('cascade');

            $table->foreign('levelId')
                    ->references('id')
                    ->on('level')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
