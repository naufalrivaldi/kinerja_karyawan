<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tglPenilaian');
            $table->date('tglApprove');
            $table->enum('status', ['1', '2', '3']);
            $table->unsignedBigInteger('periodeId');
            $table->unsignedBigInteger('penilaiId');
            $table->unsignedBigInteger('karyawanId');
            $table->timestamps();

            // fk
            $table->foreign('periodeId')
                    ->references('id')
                    ->on('periode')
                    ->onDelete('cascade');

            $table->foreign('penilaiId')
                    ->references('id')
                    ->on('penilai')
                    ->onDelete('cascade');

            $table->foreign('karyawanId')
                    ->references('id')
                    ->on('karyawan')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian');
    }
}
