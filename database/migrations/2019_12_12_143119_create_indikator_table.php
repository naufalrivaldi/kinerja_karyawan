<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndikatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indikator', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kinerja', 50);
            $table->double('bobot');
            $table->double('target');
            $table->text('detailTarget');
            $table->enum('status', ['1', '2']);
            $table->unsignedBigInteger('golonganId');

            // fk
            $table->foreign('golonganId')
                    ->references('id')
                    ->on('golongan')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indikator');
    }
}
