<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenilaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('penilai')->insert([
            [
                'nama' => 'Naufal Rivaldi',
                'nik' => 'manager',
                'password' => bcrypt('123456'),
                'remember_token' => str_random(60),
                'levelId' => 3,
                'divisiId' => 1
            ]
        ]);
    }
}
