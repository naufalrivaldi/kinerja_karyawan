<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DivisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('divisi')->insert([
            [
                'namaDivisi' => 'IT Support',
                'inisial' => 'IT'
            ],
            [
                'namaDivisi' => 'Office',
                'inisial' => 'Office'
            ],
        ]);
    }
}
